This is Unity3D 2D project [Unity 3D](https://unity.com/).

### BUILD (Unity Editor)

Add UNITY_HOME variable to your environment pointing to your Unity Editor instalation folder
```
#!bash

UNITY_HOME=/opt/unity3d/2019.2.5f1
```

### DEPLOY

## Who do I talk to?

- [dnangelus](https://github.com/DNAngeluS) repo owner and admin

- [elgambet](https://github.com/elgambet) developer and admin

- Leandro Laiño developer
