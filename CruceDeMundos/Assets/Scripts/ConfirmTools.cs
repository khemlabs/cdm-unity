﻿using UnityEngine;

public class ConfirmTools : MonoBehaviour
{
	//public GameObject btnStart;
	public GameObject confirmTools;
	public GameObject btnBack;
	public void backToSelection()
	{
		confirmTools.SetActive(false);
	}

	public void showConfirmTools()
	{
		confirmTools.SetActive(true);
	}
}
