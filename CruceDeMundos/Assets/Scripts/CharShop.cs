﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CharShop : MonoBehaviour
{

	public GameObject friend;
	public int firstChoice;
	public GameObject[] itemButtons;
	public GameObject[] toolColumn;

	int lastSelected = -1;

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < toolColumn.Length; i++)
		{
			if (i < Data.Instance.playerData.toolsNumber)
				toolColumn[i].SetActive(true);
			else
				toolColumn[i].SetActive(false);
		}
	}

	public void OnItemSelect(int itemN)
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.toolSelect);
		ShopItem ShopItem = itemButtons[itemN].GetComponent<ShopItem>();
		ToolData toolData = friend.transform.Find(ShopItem.toolName.ToString()).GetComponent<ToolData>();
		int lastCost = 0;

		//Si elegimos la última herramienta que seleccionamos nos devuelve los RTs invertidos.
		if (lastSelected == itemN)
		{
			lastCost = itemButtons[lastSelected].GetComponent<ShopItem>().val;

			Game.Instance.toolsManager.SetFriendEmpty(friend, true);

			itemButtons[itemN].GetComponent<Button>().targetGraphic.color = Color.grey;

			Transform tool = friend.transform.Find(ShopItem.toolName.ToString());
			tool.gameObject.SetActive(false);
			GameObject hb = friend.transform.Find("HealthBar").gameObject;
			hb.SetActive(false);
			friend.transform.Find("HealthBarBackground").gameObject.SetActive(false);

			Game.Instance.gameManager.resources += lastCost;
			Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
			Game.Instance.gameManager.totalInvested -= lastCost;

			Events.OnRefreshResources(Data.Instance.playerData.resources);

			lastSelected = -1;

		}
		else
		{
			//Obtenemos el último costo de la herramienta
			if (lastSelected > -1)
			{
				lastCost = itemButtons[lastSelected].GetComponent<ShopItem>().val;
			}

			if (Data.Instance.playerData.resources - ShopItem.val + lastCost >= 0)
			{

				Game.Instance.toolsManager.SetFriendEmpty(friend, true);

				foreach (GameObject item in itemButtons)
					item.GetComponent<Button>().targetGraphic.color = Color.grey;

				itemButtons[itemN].GetComponent<Button>().targetGraphic.color = Color.white;

				Transform tool = friend.transform.Find(ShopItem.toolName.ToString());
				if (!tool.gameObject.activeSelf)
				{
					string[] ttypes = System.Enum.GetNames(typeof(PlayerData.ToolName));
					for (int i = 0; i < ttypes.Length; i++)
					{
						if (ttypes[i].Equals(ShopItem.toolName.ToString()))
						{
							tool.gameObject.SetActive(true);
							GameObject hb = friend.transform.Find("HealthBar").gameObject;
							hb.SetActive(true);
							if (ttypes[i].Equals(PlayerData.ToolName.Restaurador.ToString()))
							{
								hb.GetComponent<SpriteRenderer>().sprite = hb.GetComponent<HealthBar>().portalEnergy;
							}
							else if (ttypes[i].Equals(PlayerData.ToolName.Matafuegos.ToString()))
							{
								hb.GetComponent<SpriteRenderer>().sprite = hb.GetComponent<HealthBar>().fireEnergy;
							}
							else if (ttypes[i].Equals(PlayerData.ToolName.Armonizador.ToString()))
							{
								hb.GetComponent<SpriteRenderer>().sprite = hb.GetComponent<HealthBar>().pollutionEnergy;
							}


							friend.transform.Find("HealthBarBackground").gameObject.SetActive(true);
						}
						else
						{
							friend.transform.Find(ttypes[i]).gameObject.SetActive(false);
						}
					}
				}

				toolData.CurrentLevel = toolData.levels[ShopItem.level];

				//Descuenta el nuevo costo a los recursos del jugador
				Game.Instance.gameManager.resources -= ShopItem.val;
				Data.Instance.playerData.resources = Game.Instance.gameManager.resources;

				tool.GetComponent<ShootObstacle>().UpdateTool();

				//Agrega el viejo costo a los recursos del jugador
				Game.Instance.gameManager.resources += lastCost;
				Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
				Game.Instance.gameManager.totalInvested += ShopItem.val;

				Events.OnRefreshResources(Data.Instance.playerData.resources);

				Game.Instance.toolsManager.SetFriendTool(friend, ShopItem.toolName.ToString(), ShopItem.level);

				lastSelected = itemN;
			}
		}
	}
}
