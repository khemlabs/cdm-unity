﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPlayer : MovingCharacter
{

	public bool isAvatar;
	public GameObject player;
	public GameObject friend;
	public GameObject avatar;
	public float moveSpeed = 10f;
	public float rotationSpeed = 10f;
	public float moveDistance = 2f;
	public float lostMoveDistance = 2f;
	public float lostSpeedFactor = 3f;
	bool moved = false;
	Vector3 teleportPosition;

	private readonly float rotationOffset = -90f;

	private Animator animator;
	MovingCharacter movePlayer;

	List<Vector3> pathList;
	public int pathCount;
	public Vector3 nextPoint;

	public bool lost;

	float lostDistance;

	public float RotationOffset
	{
		get
		{
			return RotationOffset2;
		}
	}

	public float RotationOffset1
	{
		get
		{
			return RotationOffset2;
		}
	}

	public float RotationOffset2
	{
		get
		{
			return rotationOffset;
		}
	}

	//List<GameObject> points;

	void Start()
	{
		animator = avatar.GetComponent<Animator>();
		if (player.activeInHierarchy)
		{
			movePlayer = player.GetComponent<MovePlayer>();
			if (movePlayer == null)
				movePlayer = player.GetComponent<FollowPlayer>();
		}
		else
		{
			movePlayer = friend.GetComponent<MovePlayer>();
			if (movePlayer == null)
				movePlayer = friend.GetComponent<FollowPlayer>();
		}
		moved = false;
		teleportPosition = new Vector3(50, -25, 0);
	}

	void FixedUpdate()
	{
		float movementDistance = moveSpeed * Time.deltaTime;
		float d = 0;
		Vector3 vectorToTarget = Vector3.zero;
		if (lost)
		{
			vectorToTarget = pathList[pathCount] - transform.position;
			d = lostMoveDistance;
		}
		else
		{
			if (player.activeInHierarchy)
			{
				vectorToTarget = player.transform.position - transform.position;
			}
			else
			{
				vectorToTarget = friend.transform.position - transform.position;
			}

			d = moveDistance;
		}

		if (vectorToTarget.magnitude > d)
		{

			Vector3 direction = Vector3.zero;
			if (lost)
			{
				transform.position = Vector3.MoveTowards(transform.position, pathList[pathCount], movementDistance * lostSpeedFactor);
				if (pathCount > 0)
					direction = pathList[pathCount - 1] - pathList[pathCount];
				else
				{
					if (player.activeInHierarchy)
					{
						direction = gameObject.transform.position - player.transform.position;
					}
					else
					{
						direction = gameObject.transform.position - friend.transform.position;
					}
				}

			}
			else
			{
				if (player.activeInHierarchy)
				{
					transform.position = Vector3.MoveTowards(transform.position, player.transform.position, movementDistance);
					direction = gameObject.transform.position - player.transform.position;
				}
				else
				{
					transform.position = Vector3.MoveTowards(transform.position, friend.transform.position, movementDistance);
					direction = gameObject.transform.position - friend.transform.position;
				}

			}


			direction.z = 0.0f;

			Quaternion newRotation = Quaternion.Slerp(avatar.transform.rotation, Quaternion.FromToRotation(Vector3.down, direction), rotationSpeed * Time.deltaTime);
			if (direction != Vector3.zero)
				avatar.transform.rotation = newRotation;
			avatar.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, avatar.transform.rotation.eulerAngles.z));

			if (vectorToTarget.magnitude > moveDistance * 3)
				StartCoroutine(CheckLostDistance());
		}
		else if (lost)
		{
			pathCount++;
			if (pathCount >= pathList.Count)
			{
				lost = false;
			}
		}

		if (movePlayer.moving && !moving)
		{
			moving = true;
			animator.Play("start");
			if (lost)
			{
				pathCount++;
				if (pathCount >= pathList.Count)
				{
					lost = false;
				}
			}
		}
		else if (!movePlayer.moving && moving)
		{
			moving = false;
			animator.Play("stop");
			if (lost)
			{
				pathCount++;
				if (pathCount >= pathList.Count)
				{
					lost = false;
				}
			}
		}

		if (Game.Instance.gameManager.state == GameManager.States.ACTIVE && Game.Instance.levelManager.leveldata.isImposible && Data.Instance.playerData.GetLevelPlayedTimes() > 4 && Data.Instance.playerData.passImposible && !moved)
		{
			transform.position = teleportPosition;
			moved = true;
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "wall" && !isAvatar && !lost)
		{
			if (player.activeInHierarchy)
			{
				lostDistance = Vector3.Distance(transform.position, player.transform.position);
			}
			else
			{
				lostDistance = Vector3.Distance(transform.position, friend.transform.position);
			}

			StartCoroutine(CheckLostDistance());
		}
	}

	IEnumerator CheckLostDistance()
	{
		yield return new WaitForSeconds(0.2f);
		float d = 0;

		if (player.activeInHierarchy)
		{
			d = Vector3.Distance(transform.position, player.transform.position);
		}
		else
		{
			d = Vector3.Distance(transform.position, friend.transform.position);
		}


		if (d > moveDistance * 1.5f)
		{
			lost = false;

			if (player.activeInHierarchy)
			{
				pathList = Game.Instance.pathfinder2.FindPath(transform.position, player.transform.position);
			}
			else
			{
				pathList = Game.Instance.pathfinder2.FindPath(transform.position, friend.transform.position);
			}


			pathCount = 0;
			yield return new WaitForSeconds(0.2f);
			if (pathList.Count > 0)
			{
				lost = true;
				//print ("lost: " + lost);
				nextPoint = pathList[pathCount];
				//points.Clear ();
				/*foreach (Vector3 p in pathList) {					
					GameObject point = GameObject.CreatePrimitive (PrimitiveType.Quad);
					point.transform.position = new Vector3 (p.x, p.y, -0.5f);
					points.Add (point);
				}*/
			}
			yield return new WaitForSeconds(0.2f);
			StartCoroutine(CheckLostDistance());
		}
		else if (d < moveDistance)
		{
			yield return new WaitForSeconds(0.2f);
			if (player.activeInHierarchy)
			{
				if (Vector3.Distance(transform.position, player.transform.position) > lostDistance)
				{
					StartCoroutine(CheckLostDistance());
				}
				else
				{
					lost = false;
				}
			}
			else
			{
				if (Vector3.Distance(transform.position, friend.transform.position) > lostDistance)
				{
					StartCoroutine(CheckLostDistance());
				}
				else
				{
					lost = false;
				}
			}

		}
		else if (lost)
		{
			yield return new WaitForSeconds(0.2f);
			StartCoroutine(CheckLostDistance());
		}
		else
		{
			yield return null;
		}
	}
}
