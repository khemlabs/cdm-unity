﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiresProgress : MonoBehaviour {

    public Text fires;

	// Use this for initialization
	void Start () {
        //En caso que el GameManager no sea null.
        if (Game.Instance.gameManager != null)
        {
            //Inicializo la cantidad de incendios que se muestra en pantalla.
            fires.text = Game.Instance.gameManager.fires.ToString();
        }
        else
        {
            //Caso contrario lo pongo en "cero"
            //tras el primer update va a tener el verdadero valor.
            fires.text = "0";
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Actualizo la cantidad de incendios que se muestra en pantalla.
        fires.text = Game.Instance.gameManager.fires.ToString();
    }
}
