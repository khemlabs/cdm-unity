﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{

	public GameObject confirm;
	public GameObject ingame;

	// Use this for initialization
	void Start()
	{
		if (SceneManager.GetActiveScene().name == "Game")
			ingame.SetActive(true);
		else
			ingame.SetActive(false);
	}

	public void Confirm()
	{
		confirm.SetActive(true);
	}

	public void Cancel()
	{
		confirm.SetActive(false);
	}

	//Salida efectiva del juego, cierra la aplicación.
	public void ConfirmExit()
	{
		if (Game.Instance != null)
		{
			Game.Instance.levelMetrics.endLevel = 2;
			Game.Instance.levelMetrics.SaveLevelData("");
		}

		Data.Instance.dataController.RetrySendData();
		if (Game.Instance != null)
		{
			Game.Instance.gameManager.resources += Game.Instance.gameManager.totalInvested;
			Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
		}
		Events.OnRefreshResources(Data.Instance.playerData.resources);
		Data.Instance.Exit();
	}

	//Salida al mapa de niveles.
	public void ExitToMap()
	{
		SceneManager.LoadScene("LevelMap");
		//endLevel 9 es la salida del nivel al mapa
		Game.Instance.levelMetrics.endLevel = 9;
		Game.Instance.gameManager.resources += Game.Instance.gameManager.totalInvested;
		Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
		Events.OnRefreshResources(Data.Instance.playerData.resources);
		Game.Instance.levelMetrics.SaveLevelData("");
	}
}
