﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.Audio;
using System;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;

/// <summary>
///  De aqui se cargan los niveles y toda la informacion relacioanda a ellos. en caso de necesitar editar alguno ir a  
///  ./Resourses/Data.prefab desde el proyecto Unity, aqui ir a la seccion LevelData y desplegar el listado "levels" esto 
///  nos permite agregar quitar y modificar niveles.
/// </summary>
public class Data : MonoBehaviour
{
	const string PREFAB_PATH = "Data";
	static Data mInstance = null;
	public static DataConfigFile ConfigFile = new DataConfigFile();
	public static bool Loading = true;
	public string baseURL;
	public string serverURL;
	public float gameTimeInit;

	public string userId;
	public string userName;
	public PlayerData playerData;
	public DialogData dialogData;
	public LevelData levelData;

	public DataController dataController;

	public AvatarData avatarData;

	public CSDialogData csdialogData;

	public MusicManager musicManager;
	public InterfaceSfx interfaceSfx;
	public AudioMixer audioMaster;
	public bool mute;

	public bool unlockAllLevels;

	public string cue;
	public string grado;
	public string division;
	public string turno;

	private Fade fade;

	//======================================================================
	public bool SyncServerMode = false;
	//======================================================================
	public Data()
	{
		baseURL = ConfigFile.clientBaseURL;
		serverURL = ConfigFile.serverBaseURL;
	}
	public static Data Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = FindObjectOfType<Data>();

				if (mInstance == null)
				{
					GameObject go = Instantiate(Resources.Load<GameObject>(PREFAB_PATH)) as GameObject;
					mInstance = go.GetComponent<Data>();
					go.transform.localPosition = new Vector3(0, 0, 0);
				}
			}
			return mInstance;
		}
	}

	IEnumerator loadComponents()
	{
		yield return new WaitUntil(() => !Loading);

		playerData = GetComponent<PlayerData>();
		dialogData = GetComponent<DialogData>();
		levelData = GetComponent<LevelData>();
		dataController = GetComponent<DataController>();
		avatarData = GetComponent<AvatarData>();
		csdialogData = GetComponent<CSDialogData>();
		musicManager = GetComponent<MusicManager>();
		interfaceSfx = GetComponentInChildren<InterfaceSfx>();
		fade = GetComponentInChildren<Fade>();
	}

	IEnumerator updateServerConfig()
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		UnityWebRequest serverConfigRequest = UnityWebRequest.Get(baseURL + "/ServerConfig.json");
#else
		UnityWebRequest serverConfigRequest = UnityWebRequest.Get("file://" + Directory.GetCurrentDirectory() + "/ServerConfig.json");
#endif
		// new WaitUntil(() => .isDone)
		yield return serverConfigRequest.SendWebRequest();

		if (serverConfigRequest.isNetworkError || serverConfigRequest.isHttpError)
		{
			Debug.LogError(serverConfigRequest.error);
		}
		else
		{
			var configJson = JsonConvert.DeserializeObject<DataConfigFile>(serverConfigRequest.downloadHandler.text);
			serverURL = configJson.serverBaseURL;
			baseURL = configJson.clientBaseURL;
		}
		Loading = false;
	}

	private void Start()
	{
		Events.OnUserUpdate += OnUserUpdate;
	}

	void OnApplicationFocus(bool hasFocus)
	{
		Mute(!hasFocus);
	}

	public void LoadLevel(string aLevelName)
	{
		musicManager.MusicChange(aLevelName);
		LoadLevel(aLevelName, 0.01f, 0.01f, Color.black);
	}
	public void LoadLevel(string aLevelName, float aFadeOutTime, float aFadeInTime, Color aColor)
	{
		musicManager.MusicChange(aLevelName);
		fade.LoadLevel(aLevelName, aFadeOutTime, aFadeInTime, aColor);

	}
	void Awake()
	{
		if (!mInstance)
			mInstance = this;
		else
		{
			Destroy(this.gameObject);
			return;
		}
		StartCoroutine(updateServerConfig());
		StartCoroutine(loadComponents());

		DontDestroyOnLoad(this.gameObject);
	}

	public void OnUserUpdate(string user)
	{
		userId = user.ToUpper();
	}

	public void SaveDialogData(string character, int level, int index, string mood, int answerId, float time, int dialogId)
	{
		StartCoroutine(dataController.SaveDialogData(userId.ToUpper(), SystemInfo.deviceUniqueIdentifier, character, level, index, mood, answerId, time, dialogId));
	}

	public void SaveLevelData(string tools, string toolsEnd, string misions, int portalDone, int fireDone, int pollutionDone, int mapchecks,
		float levelTime, float gameTime, float firstMapTime, float toolsMapTime, float phoneMapTime, float misionTime, float toolsTime, string mapTrail, string mapDeadEnds, int rtB,
		int rtAt, int pCharge, int fCharge, int poCharge, int rtCharge, int giveup)
	{
		StartCoroutine(dataController.SaveLevelData(userId.ToUpper(), SystemInfo.deviceUniqueIdentifier, playerData.level > 7 ? 7 : playerData.level, tools, toolsEnd, misions, portalDone, fireDone, pollutionDone, mapchecks,
			levelTime, gameTime, firstMapTime, toolsMapTime, phoneMapTime, misionTime, toolsTime, mapTrail, mapDeadEnds, rtB, rtAt, playerData.resources, pCharge, fCharge,
			poCharge, rtCharge, giveup));
	}

	public void SaveUserData(string user)
	{
		StartCoroutine(dataController.CreateUserRoutine(user.ToUpper(), SystemInfo.deviceUniqueIdentifier, userName, cue, grado, division, turno));
	}

	public void SaveSelfie(string emoji, string estado)
	{
		StartCoroutine(dataController.SaveSelfie(userId, SystemInfo.deviceUniqueIdentifier, playerData.level, emoji, estado));
	}

	/// <summary>
	/// 	Se levantan los datos del usuario que haya guardado localmente.
	/// </summary>
	public async Task LoadUserDataWeb(string userID)
	{
		var saveUtil = new SaverUtil();
		var response = await saveUtil.loadDataWeb(serverURL + "/user/" + userID.ToLower());

		if (!response.Equals(""))
		{
			User user = JsonConvert.DeserializeObject<User>(response);

			userId = user.getUserId();
			userName = user.getUserName();
		}
	}

	public void LoadUserData(string userID)
	{
		var saveUtil = new SaverUtil();
		var response = saveUtil.loadData(userID.ToUpper() + "UserData");

		if (!response.Equals(""))
		{
			User user = JsonConvert.DeserializeObject<User>(response);

			userId = user.getUserId();
			userName = user.getUserName();
		}
	}

	/// <summary>
	/// 	Aquí se guarda los datos de juego del jugador localmente
	/// </summary>
	public void SaveGameData()
	{
		SendData();
		string json;

		GameDataInfo gameDataInfo = new GameDataInfo(playerData.GetPlayerData(), dialogData.GetDialogData());

		json = JsonConvert.SerializeObject(gameDataInfo);
		SaverUtil saveUtil = new SaverUtil();

		saveUtil.saveData(userId.ToUpper() + "GameData", json);
	}

	public async Task SaveWebGameData()
	{
		SendData();
		string json;

		GameDataInfo gameDataInfo = new GameDataInfo(playerData.GetPlayerData(), dialogData.GetDialogData());

		json = JsonConvert.SerializeObject(gameDataInfo);
		SaverUtil saveUtil = new SaverUtil();

		await saveUtil.saveDataWeb(serverURL + "/game/" + userId.ToLower(), json);
	}

	//Aqui levantamos los datos de la partida del jugador desde servidor
	public async Task LoadWebGameData(string user)
	{
		SaverUtil saveUtil = new SaverUtil();
		string response = await saveUtil.loadDataWeb(serverURL + "/game/" + user.ToLower());

		if (response != "")
		{
			GameDataInfo gameDataInfo = JsonConvert.DeserializeObject<GameDataInfo>(response);
			playerData.SetPlayerData(gameDataInfo.playerInfo);
			dialogData.SetDialogData(gameDataInfo.dialogResponseInfo);
		}
		else
		{
			SaveGameData();
		}
	}

	//Aqui levantamos los datos de la partida del jugador desde local
	public void LoadGameData(string user)
	{
		string response = "";
		SaverUtil saveUtil = new SaverUtil();

		//Leemos los datos de la ultima partida jugada
		response = saveUtil.loadData(user.ToUpper() + "GameData");

		if (!response.Equals(""))
		{
			GameDataInfo gameDataInfo = JsonConvert.DeserializeObject<GameDataInfo>(response);
			playerData.SetPlayerData(gameDataInfo.playerInfo);
			dialogData.SetDialogData(gameDataInfo.dialogResponseInfo);
		}
		else
		{
			SaveGameData();
		}

	}

	public void Reset()
	{
		PlayerPrefs.DeleteAll();
	}

	public void SendData()
	{
		dialogData.ResetHintAtLevel(playerData.level);
	}

	public void Mute(bool m)
	{
		mute = m;
		if (mute)
			audioMaster.SetFloat("masterVolume", -80f);
		else
			audioMaster.SetFloat("masterVolume", 0f);
	}

	public string GetFullPathByFolder(string FolderName, string fileName)
	{
		string folder = Path.Combine(Application.persistentDataPath, FolderName);
		if (!Directory.Exists(folder))
			Directory.CreateDirectory(folder);
		return Path.Combine(folder, fileName);
	}

	public void Exit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
		Application.OpenURL(webplayerQuitURL);
#else
		Application.Quit();
#endif
	}
}
