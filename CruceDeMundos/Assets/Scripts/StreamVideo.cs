﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour
{
	//public RawImage rawImage;
	public VideoPlayer videoPlayer;
	bool Playing;

	// Start is called before the first frame update
	void Start()
	{
		Playing = true;
		StartCoroutine(PlayVideo());
	}

	IEnumerator PlayVideo()
	{
		videoPlayer.Prepare();

		WaitForSeconds waitForSeconds = new WaitForSeconds(1);

		//Hacemos una espera hasta que el reproductor este listo
		while (!videoPlayer.isPrepared)
		{
			yield return waitForSeconds;
			break;
		}

		//ponemos a reproducir el video
		videoPlayer.Play();

		//Validamos que se reproduce el video
		while (videoPlayer.isPlaying)
		{
			Playing = true;
			yield return null;
		}
		Playing = false;

		//una vez que termina el video arrancamos el juego
		if (!Playing)
		{
			if (Data.Instance.playerData.level == 11)
			{
				Data.Instance.LoadLevel("Fin", 1f, 3f, Color.black);
			}
			else
			{
				if (Data.Instance.playerData.level == 1)
				{
					Data.Instance.LoadLevel("LevelMap", 1f, 3f, Color.black);
				}
				else
				{
					Data.Instance.LoadLevel("Game", 1f, 3f, Color.black);
				}
			}
		}
	}
}
