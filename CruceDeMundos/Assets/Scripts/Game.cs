﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{

	private States lastState;
	public States state;
	public GameUI gameUI;
	public GameManager gameManager;
	public DialogManager dialogManager;
	public CharacterManager characterManager;
	public LevelManager levelManager;
	public ToolsManager toolsManager;
	public PathFinder pathfinder;
	public PathFinder2 pathfinder2;
	public TraceManager traceManager;
	public LevelMetrics levelMetrics;
	public IngameMusic ingameMusic;
	public IngameSfx ingameSfx;
	public GameObject friend1;
	public GameObject friend2;
	public string userID;

	public GameObject fade;
	[HideInInspector]
	public float globalGlow;


	public enum States
	{
		PAUSED,
		PLAYING,
		ENDED
	}

	static Game mInstance = null;

	public static Game Instance
	{
		get
		{
			if (mInstance == null)
			{
			}
			return mInstance;
		}
	}
	void Awake()
	{
		mInstance = this;
		traceManager = GetComponent<TraceManager>();
	}
	void Start()
	{
		gameManager = GetComponent<GameManager>();
		dialogManager = GetComponent<DialogManager>();
		levelManager = GetComponent<LevelManager>();
		toolsManager = GetComponent<ToolsManager>();
		pathfinder = GetComponent<PathFinder>();
		pathfinder2 = GetComponent<PathFinder2>();

		levelMetrics = GetComponent<LevelMetrics>();

		ingameMusic = GetComponentInChildren<IngameMusic>();
		ingameSfx = GetComponentInChildren<IngameSfx>();

		FadeIn(2f, true);
	}

	void StartGame()
	{
		state = States.PLAYING;
	}

	void OnLevelComplete()
	{
		state = States.PAUSED;
	}

	void OnGameOver()
	{
		state = States.PAUSED;
	}

	public void NextLevel()
	{
		OnGamePaused(false);
		Data.Instance.playerData.level++;
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		if (Game.Instance.levelManager.leveldata.isImposible && Game.Instance.levelMetrics.endLevel == 7)
		{
			Game.Instance.levelMetrics.endLevel = 4;

			Data.Instance.playerData.weCanPass = false;
			Data.Instance.playerData.passImposible = false;
		}

		if (Data.Instance.playerData.level == 4)
			Data.Instance.LoadLevel("Cutscene", 1f, 3f, Color.black);
		else if (Data.Instance.playerData.level > 10)
		{
			Data.Instance.LoadLevel("LevelMap");
		}
		else
			Data.Instance.LoadLevel("Game");

		if (Data.Instance.playerData.level == 8)
		{
			//Carga la escena del video final
			Data.Instance.LoadLevel("VideoFinal", 1f, 3f, Color.black);
		}

	}

	public void Replay()
	{
		OnGamePaused(false);
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		if (Game.Instance.levelManager.leveldata.isImposible)
		{
			Data.Instance.playerData.imposibleAttempts = Game.Instance.gameManager.imposibleAttempts;

		}

		Data.Instance.playerData.weCanPass = false;
		Data.Instance.playerData.passImposible = false;

		Data.Instance.LoadLevel("Game");
	}

	public void GiveUp()
	{
		OnGamePaused(false);
		if (Game.Instance.levelManager.leveldata.isImposible)
		{
			if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 4;
		}
		Game.Instance.levelMetrics.levelTime = Game.Instance.gameUI.timeprogress.time;
		Data.Instance.playerData.UnlockNext();

		if (!Game.Instance.gameUI.dataSent)
		{
			Game.Instance.levelMetrics.SaveLevelData("");
		}

		Data.Instance.playerData.SetSummary();

		if (Data.Instance.SyncServerMode)
		{
			saveGameAsync();
		}
		else
		{
			Data.Instance.SaveGameData();
		}

		Data.Instance.playerData.level++;
		Data.Instance.LoadLevel("Game");
	}
	async void saveGameAsync()
	{
		await Data.Instance.SaveWebGameData();
	}

	public async void LevelMap()
	{
		if (gameManager.state == GameManager.States.MAP)
		{
			Game.Instance.levelMetrics.levelEndTime = Time.realtimeSinceStartup;
			Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
			Game.Instance.gameManager.resources += Game.Instance.gameManager.totalInvested;
			Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
			Events.OnRefreshResources(Data.Instance.playerData.resources);
			await Data.Instance.LoadWebGameData(userID);
		}
		else
		{
			if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 1;
		}


		if (!Game.Instance.gameUI.dataSent)
		{
			Game.Instance.levelMetrics.SaveLevelData("");
		}

		OnGamePaused(false);
		Data.Instance.LoadLevel("LevelMap");
	}

	/// <summary>
	///     Pausa o reanuda juego y el tiempo transcurrido dependiendo del parametro que pasemos
	/// </summary>
	/// <param name="paused">parametro de pausa</param>
	public void OnGamePaused(bool paused)
	{
		if (paused)
		{
			lastState = state;
			Time.timeScale = 0;
			state = States.PAUSED;
		}
		else
		{
			state = lastState;
			Time.timeScale = 1;
		}
	}

	public void OnApplicationQuit()
	{
		if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 3;
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		Game.Instance.gameManager.resources += Game.Instance.gameManager.totalInvested;
		Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
		Events.OnRefreshResources(Data.Instance.playerData.resources);

		if (!Game.Instance.gameUI.dataSent)
		{
			Game.Instance.levelMetrics.SaveLevelData("");
		}

		Data.Instance.dataController.RetrySendData();
	}

	public void FadeIn(float seconds, bool pingpong)
	{
		GameObject f = Instantiate(fade);
		f.transform.parent = Data.Instance.gameObject.transform;
		Fade fadeIn = f.GetComponent<Fade>();

		fadeIn.OnLoopMethod = () =>
		{
			globalGlow = Mathf.Lerp(0.3f, 1f, fadeIn.time);
		};
		fadeIn.OnEndMethod = () =>
		{
			if (pingpong)
				FadeOut(seconds, pingpong);
			fadeIn.Destroy();
		};
		fadeIn.StartFadeIn(seconds);
	}

	public void FadeOut(float seconds, bool pingpong)
	{
		GameObject f = Instantiate(fade);
		f.transform.parent = Data.Instance.gameObject.transform;
		Fade fadeOut = f.GetComponent<Fade>();

		fadeOut.OnLoopMethod = () =>
		{
			globalGlow = Mathf.Lerp(0.3f, 1f, fadeOut.time);
		};
		fadeOut.OnEndMethod = () =>
		{
			if (pingpong)
				FadeIn(seconds, pingpong);
			fadeOut.Destroy();
		};
		fadeOut.StartFadeOut(seconds);
	}
}
