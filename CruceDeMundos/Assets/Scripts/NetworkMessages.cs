﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class NetworkMessages : MonoBehaviour
{
	public GameObject networkMessages;

	public Text text;
	public Button btnOk;

	void Awake()
	{
		Debug.Log("Awake");
		text = GetComponentInChildren<Text>();
		btnOk = GetComponentInChildren<Button>();
	}

	public void Ok()
	{
		networkMessages.SetActive(false);
	}

	public void setMessage(string message)
	{
		text.text = message;
	}

	public void show()
	{
		networkMessages.SetActive(true);
	}

	public void hide()
	{
		networkMessages.SetActive(false);
	}

	public void enableOk()
	{
		btnOk.gameObject.SetActive(true);
		btnOk.interactable = true;
	}
	public void disableOk()
	{
		btnOk.gameObject.SetActive(true);
		btnOk.interactable = false;
	}
}
