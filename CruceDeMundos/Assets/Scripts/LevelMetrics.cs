﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMetrics : MonoBehaviour
{

	public int mapCheck;
	public string tools;
	public string toolsEndCharge;
	public float levelBeginTime;
	public float levelEndTime;
	public float levelTime;

	public float map1BeginTime;
	public float map1EndTime;
	public float phoneMapTime;
	public float firstMapTime;
	public float toolsMapTime;

	public float objectivesBeginTime;
	public float objectivesEndTime;

	public float toolsBeginTime;
	public float toolsEndTime;

	public string trail;
	public string deadEnds;

	public int rtBegin;
	public int rtPostTools;

	public int endLevel = 0;

	public int portalesCerrados;
	public int fuegosApagados;
	public int polucionEliminada;

	public int portalCharge;
	public int fireCharge;
	public int pollutionCharge;
	public int resourcesCharge;

	public float map2BeginTime;

	// Use this for initialization
	void Start()
	{
		Events.OnObstacleDestroy += OnObstacleDestroy;
	}

	void OnDestroy()
	{
		Events.OnObstacleDestroy -= OnObstacleDestroy;
	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnObstacleDestroy(string tag)
	{
		if (tag == ShootObstacle.ObstacleType.PORTAL.ToString())
		{
			portalesCerrados++;
		}
		else if (tag == ShootObstacle.ObstacleType.FIRE.ToString())
		{
			fuegosApagados++;
		}
		else if (tag == ShootObstacle.ObstacleType.POLLUTION.ToString())
		{
			polucionEliminada++;
		}
	}

	public void SaveLevelData(string misions)
	{
		Events.GetVisistedTrail();
		if (Game.Instance.gameManager.state != GameManager.States.MAP)
		{
			Game.Instance.toolsManager.FinalToolsCharge();
		}

		Data.Instance.SaveLevelData(tools,
																toolsEndCharge,
																misions,
																portalesCerrados,
																fuegosApagados,
																polucionEliminada,
																mapCheck,
																levelTime,
																Time.realtimeSinceStartup - Data.Instance.gameTimeInit,
																firstMapTime,
																toolsMapTime,
																phoneMapTime,
																objectivesEndTime - objectivesBeginTime,
																toolsEndTime - toolsBeginTime,
																trail,
																deadEnds,
																rtBegin,
																rtPostTools,
																portalCharge,
																fireCharge,
																pollutionCharge,
																resourcesCharge,
																endLevel);

	}

}
