﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;

[Serializable]
public class DataConfigFile
{
	public string serverBaseURL;
	public string clientBaseURL;

	public DataConfigFile()
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		// Debug.Log("Load variables");
		// Debug.Log("String " + EnvironmentScript.GetEnvironmentJsonString());
		var Env = new Hashtable();
		// var Env = JsonUtility.FromJson<Hashtable>(EnvironmentScript.GetEnvironmentJsonString());
		// Debug.Log("ENV " + Env.ToString());
#else
		var Env = Environment.GetEnvironmentVariables();
#endif
		serverBaseURL = Env["CDM_SERVER"] != null ? Env["CDM_SERVER"].ToString() : "https://cdm.khem.io";
		clientBaseURL = Env["CDM_CLIENT"] != null ? Env["CDM_CLIENT"].ToString() : "https://cdm.khem.io";
	}
	public DataConfigFile(string serverBaseURL, string clientBaseURL)
	{
		this.serverBaseURL = serverBaseURL;
		this.clientBaseURL = clientBaseURL;
	}
}
