﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Posible respuesta para almacenar
/// </summary>
[Serializable]
public class Response{
    public string characterName;
    public int level;
    public int dialogIndex;
    public string mood;
    public int answerId;
    public float timeResponse;
}
