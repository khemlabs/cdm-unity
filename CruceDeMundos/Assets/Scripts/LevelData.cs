﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

public class LevelData : MonoBehaviour
{

	public bool saveToDatabase;

	public List<Level> levels;

	// Use this for initialization
	void Start()
	{
		if (saveToDatabase)
		{
			StringBuilder builder = new StringBuilder();
			string m = "";
			foreach (Level l in levels)
			{
				builder.Append(m);
				foreach (string o in l.objectives)
				{
					builder.Append(o);
					builder.Append(";");
				}
				m = builder.ToString();
				if (l.levelNumber > 0)
					StartCoroutine(Data.Instance.dataController.AddLevelData(l.levelNumber, m, l.portalNumber, l.fireNumber, l.pollutionNumber, l.portalCharge, l.fireCharge, l.pollutionCharge, l.resourcesCharge));
				m = "";
			}
		}
#if UNITY_EDITOR
		StringBuilder builderLevel = new StringBuilder();

		//(`id`, `level_id`, `missions`, `portal`, `fire`, `pollution`, `create_time`, `portal_charge`, `fire_charge`, `pollution_charge`, `resources_charge`) 
		foreach (var level in levels)
		{
			builderLevel.Append("(");
			builderLevel.Append(level.levelNumber);
			builderLevel.Append(",");
			builderLevel.Append(level.levelNumber);
			builderLevel.Append(",");
			int i = 1;
			foreach (string objective in level.objectives)
			{
				builderLevel.Append(objective);
				if (i < level.objectives.Count) builderLevel.Append(";");
				i++;
			}

			builderLevel.Append(",");
			builderLevel.Append(level.portalNumber);
			builderLevel.Append(",");
			builderLevel.Append(level.fireNumber);
			builderLevel.Append(",");
			builderLevel.Append(level.pollutionNumber);
			builderLevel.Append(",");
			builderLevel.Append(level.portalCharge);
			builderLevel.Append(",");
			builderLevel.Append(level.fireCharge);
			builderLevel.Append(",");
			builderLevel.Append(level.pollutionCharge);
			builderLevel.Append(",");
			builderLevel.Append(level.resourcesCharge);
			builderLevel.Append(")\n");
		}

		string path = "./fullLevels.json";
		if (!File.Exists(path))
		{
			File.WriteAllText(path, builderLevel.ToString());
		}
#endif
	}

	[Serializable]
	public class Level
	{
		public int levelNumber;
		public string title;
		public List<string> objectives;
		public bool isVsTime;
		public float timeOut;
		public int fireNumber;
		public int portalNumber;
		public int pollutionNumber;
		public int fireCharge;
		public int portalCharge;
		public int pollutionCharge;
		public int resourcesCharge;
		public List<TimeObjective> timeObjectives;
		public List<ObstacleObjective> obstacleObjectives;
		public List<ChargeObjective> chargeObjective;
		public List<DialogObjective> dialogObjective;
		public List<DialogUnlock> dialogsUnlock;
		public List<CharacterMove> characterMove;
		public GameObject levelObjects;
		public CameraController.Zoom zoomIntro;
		public CameraController.Zoom zoomIn;
		public CameraController.Zoom zoomOut;
		public string layoutJson;
		public VisualCell labCell;
		public int resourceWin;
		public bool isTuto;
		public bool isImposible;
	}

	[Serializable]
	public class ObstacleObjective
	{
		public ShootObstacle.ObstacleType tag;
		public int number;
		public int objectiveIndex;
	}

	[Serializable]
	public class ChargeObjective
	{
		public PlayerData.ToolName toolName;
		public int val;
		public int objectiveIndex;
	}

	[Serializable]
	public class DialogObjective
	{
		public string characterName;
		public int objectiveIndex;
	}

	[Serializable]
	public class TimeObjective
	{
		public float timeOut;
		public int objectiveIndex;
	}

	[Serializable]
	public class DialogUnlock
	{
		public string characterName;
		public int goTo;
		public List<ObstacleObjective> obstacleObjectives;
	}

	[Serializable]
	public class CharacterMove
	{
		public string characterName;
		public Vector2[] positions;
	}
}
