﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PollutionsProgress : MonoBehaviour {

    public Text pollutions;

	// Use this for initialization
	void Start () {
        //En caso que el GameManager no sea null.
        if (Game.Instance.gameManager != null)
        {
            //Inicializo la cantidad de contaminantes que se muestra en pantalla.
            pollutions.text = Game.Instance.gameManager.pollutions.ToString();
        }
        else
        {
            //Caso contrario lo pongo en "cero"
            //tras el primer update va a tener el verdadero valor.
            pollutions.text = "0";
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Actualizo la cantidad de contaminantes que se muestra en pantalla.
        pollutions.text = Game.Instance.gameManager.pollutions.ToString();
    }
}
