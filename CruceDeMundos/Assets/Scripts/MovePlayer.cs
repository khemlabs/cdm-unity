﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MovingCharacter
{

	public GameObject avatar;
	public float speed = 15f;
	public bool started;
	Rigidbody2D rb;
	Vector2 teleport = new Vector2(45, -25);

	private readonly float rotationOffset = -90f;
	private Animator animator;
	private AudioSource source;
	public AudioClip ring;
	public GameObject tutorial;
	Tutorial tutorialScript;
	bool moved;
	bool answered;
	bool takingDesition = false;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		Game.Instance.traceManager.freeTrail = true;
		animator = avatar.GetComponent<Animator>();

		source = GetComponent<AudioSource>();
		answered = false;
		moved = false;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		tutorialScript = tutorial.GetComponent(typeof(Tutorial)) as Tutorial;
		if (Game.Instance.gameManager.state == GameManager.States.ACTIVE && (!tutorialScript.showingTutorial || !tutorialScript.showingReminder) && !takingDesition)
		{
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");

			if (moveHorizontal != 0 || moveVertical != 0)
			{
				if (!moving)
				{
					animator.Play("start");
					moving = true;
					source.PlayOneShot(ring);
				}
			}
			else
			{
				if (moving)
				{
					animator.Play("stop");
					moving = false;
				}
			}

			Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0f);
			RotateIntoMoveDirection(moveHorizontal, moveVertical);

			// Normalise the movement vector and make it proportional to the speed per second.
			movement = movement.normalized * speed * Time.deltaTime;
			// Move the player to it's current position plus the movement.
			rb.MovePosition(transform.position + movement);
		}

		//Disparo el dialogo de la Dra. para que me telestransporte
		if (Game.Instance.levelManager.leveldata.isImposible && Data.Instance.playerData.GetLevelPlayedTimes() > 4 && Game.Instance.gameUI.ingameUI.activeSelf && Vector3.Distance(rb.position, teleport) > 20 && !Data.Instance.playerData.passImposible && !Data.Instance.playerData.weCanPass)
		{
			Game.Instance.gameUI.ingameUI.SetActive(false);
			Game.Instance.gameUI.dialog.SetActive(true);
			Game.Instance.dialogManager.LoadDialog("Dra Grimberg");
			takingDesition = true;
		}

		//Si acepta el jugador lo mueve de lugar
		if (Data.Instance.playerData.passImposible && !moved)
		{
			rb.position = teleport;
			moved = true;
		}

		//Quito el dialogo y muestro el HUD normal
		if (Game.Instance.levelManager.leveldata.isImposible && Data.Instance.playerData.GetLevelPlayedTimes() > 4 && (Data.Instance.playerData.passImposible || Data.Instance.playerData.weCanPass) && !answered)
		{
			Game.Instance.gameUI.ingameUI.SetActive(true);
			Game.Instance.gameUI.dialog.SetActive(false);
			takingDesition = false;
			answered = true;
		}
	}


	private void RotateIntoMoveDirection(float x, float y)
	{
		if (x != 0f || y != 0f)
		{
			float rotationAngle = rotationOffset + Mathf.Atan2(y, x) * 180 / Mathf.PI;
			//GameObject sprite = (GameObject)gameObject.transform.FindChild("Sprite").gameObject;
			avatar.transform.rotation = Quaternion.Euler(0f, 0f, rotationAngle);
		}

	}

	public float DistanceToGoal()
	{
		return 0f;
	}

}
