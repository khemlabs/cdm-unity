﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Threading.Tasks;

public class AvatarData : MonoBehaviour
{
	public int pielIndex;
	public int peloIndex;
	public int peloColorIndex;
	public int caraIndex;
	public int torsoIndex;
	public int piernasIndex;
	public int zapatoIndex;
	public int estadoIndex;
	public Texture2D selfie;
	public int selfie_w;
	public int selfie_h;
	public List<string> estados;
	public string UserID;
	string PATH = "/Selfie";

	// Use this for initialization
	void Start()
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		StartCoroutine(Import("estados.json"));
#else
		DirectoryInfo dir = new DirectoryInfo(Directory.GetParent(Application.dataPath).FullName + PATH);

		FileInfo[] info = dir.GetFiles("*.json");

		foreach (FileInfo f in info)
		{
			if (!f.FullName.Contains("fulldialogs"))
			{
				StartCoroutine(Import(f.FullName));
			}
		}
#endif
	}

	IEnumerator Import(string file)
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		string URL = Data.Instance.baseURL;
		UnityWebRequest www = UnityWebRequest.Get(URL + "/Selfie/" + file);
#else
		UnityWebRequest www = UnityWebRequest.Get("file://" + file);
#endif
		yield return www.SendWebRequest();
		string text = "";

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			//text = www.downloadHandler.text;
			text = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data, 3, (int)www.downloadedBytes - 3);
		}

		// SelfieStates states = JsonConvert.DeserializeObject<SelfieStates>(text);
		SelfieStates states = JsonUtility.FromJson<SelfieStates>(text);

		for (int i = 0; i < states.estados.Count; i++)
		{
			estados.Add(states.estados[i]);
		}
	}

	//Aquí se guarda los datos del avatar del jugador
	public void SaveAvatarData(string userId)
	{
		Avatar avatar = new Avatar(pielIndex, peloIndex, peloColorIndex, caraIndex, torsoIndex, piernasIndex, zapatoIndex, estadoIndex);

		//Se guardan los datos del avatar
		new SaverUtil().saveData(userId.ToUpper() + "avatarData", JsonConvert.SerializeObject(avatar));
	}

	//Aquí se cargan los datos del avatar del jugador
	public async Task LoadAvatarDataWeb(string user)
	{
		string URL = Data.Instance.serverURL;
		var saveUtil = new SaverUtil();
		var response = await saveUtil.loadDataWeb(URL + "/avatar/" + user.ToLower());

		Avatar avatar = JsonConvert.DeserializeObject<Avatar>(response);

		if (!response.Equals(""))
		{
			pielIndex = avatar.getPielIndex();
			peloIndex = avatar.getPeloIndex();
			peloColorIndex = avatar.getPeloColorIndex();
			caraIndex = avatar.getCaraIndex();
			torsoIndex = avatar.getTorsoIndex();
			piernasIndex = avatar.getPiernasIndex();
			zapatoIndex = avatar.getZapatoIndex();
			estadoIndex = avatar.getEstadoIndex();
		}

		LoadSelfieImg();
	}

	public void LoadAvatarData(string user)
	{
		SaverUtil saveUtil = new SaverUtil();
		string response = saveUtil.loadData(user.ToUpper() + "AvatarData");

		Avatar avatar = JsonConvert.DeserializeObject<Avatar>(response);

		if (!response.Equals(""))
		{
			pielIndex = avatar.getPielIndex();
			peloIndex = avatar.getPeloIndex();
			peloColorIndex = avatar.getPeloColorIndex();
			caraIndex = avatar.getCaraIndex();
			torsoIndex = avatar.getTorsoIndex();
			piernasIndex = avatar.getPiernasIndex();
			zapatoIndex = avatar.getZapatoIndex();
			estadoIndex = avatar.getEstadoIndex();
		}

		LoadSelfieImg();
	}

	public void CaptureSelfie(RenderTexture rt)
	{
		selfie = new Texture2D(rt.width, rt.height);
		RenderTexture.active = rt;
		selfie.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
		selfie.Apply();

		byte[] bytes = selfie.EncodeToPNG();
		string filename = Data.Instance.GetFullPathByFolder("img", Data.Instance.userId.ToUpper() + "selfie.png");
		System.IO.File.WriteAllBytes(filename, bytes);
	}

	void LoadSelfieImg()
	{
		Texture2D tex = null;
		byte[] fileData;

		string filename = Data.Instance.GetFullPathByFolder("img", Data.Instance.userId.ToUpper() + "selfie.png");

		if (File.Exists(filename))
		{
			fileData = File.ReadAllBytes(filename);
			tex = new Texture2D(2, 2);
			tex.LoadImage(fileData);
			selfie = tex;
		}
	}
}
