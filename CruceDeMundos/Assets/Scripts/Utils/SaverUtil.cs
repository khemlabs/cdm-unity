﻿using System.Collections;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class SaverUtil
{
	public async Task<string> saveDataWeb(string url, string postData)
	{
		//Guardado en el servidor
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, postData))
		{
			// Pedido de guardado de información.
			await webRequest.SendWebRequest();

			if (webRequest.isHttpError || webRequest.isNetworkError)
			{
				return webRequest.error;
			}
			else
			{
				return webRequest.downloadHandler.text;
			}
		}
	}
	public void saveData(string key, string json)
	{
		//Verificamos que la key no exista y guardamos
		if (!PlayerPrefs.HasKey(key))
		{
			PlayerPrefs.SetString(key, json);
		}
		else
		{
			//En caso de que exista la key validamos que la información sea distinta,
			//de ser así guardamos información
			if (PlayerPrefs.GetString(key) != json)
			{
				PlayerPrefs.SetString(key, json);
			}
		}
	}

	public async Task<string> loadDataWeb(string url)
	{
		//Guardado en LocalStorage del Browser
		using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
		{
			// Pedido de carga de información
			await webRequest.SendWebRequest();

			if (webRequest.isHttpError || webRequest.isNetworkError)
			{
				return webRequest.error;
			}
			else
			{
				return webRequest.downloadHandler.text;
			}
		}
	}

	public string loadData(string key)
	{
		return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) : "";
	}
}