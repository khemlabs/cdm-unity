﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.IO;
using System;

public class Sign : MonoBehaviour
{

	public Text userName;
	public Camera selfieCam;
	public RenderTexture selfieRT;
	public Button continuar;
	public RawImage avatarImage;

	public string cue;
	public string grado;
	public string division;
	public string turno;
	bool apodoDone, screenshot, nextScene;

	void Update()
	{
		if (screenshot)
		{
			avatarImage.enabled = false;
			selfieCam.enabled = true;
			nextScene = true;
			screenshot = false;
		}
		else if (nextScene)
		{
			if (nextScene)
			{
				selfieCam.enabled = false;
				Data.Instance.avatarData.CaptureSelfie(selfieRT);
				//Carga la escena del video de presentación
				Data.Instance.LoadLevel("Video", 1f, 3f, Color.black);
				nextScene = false;
			}
		}
	}

	public void Continue()
	{
		selfieCam.transform.position = new Vector3(46f, 2.5f, -9f);
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		Data.Instance.userName = userName.text;
		string user = Data.Instance.userId.ToUpper();

		Data.Instance.SaveUserData(user);
		Data.Instance.avatarData.SaveAvatarData(user);

		screenshot = true;
	}

	public void SetApodo(string s)
	{
		apodoDone = s == "" ? false : true;
		SetContinue();
	}

	void SetContinue()
	{
		if (apodoDone)
		{
			continuar.interactable = true;
		}
		else
		{
			continuar.interactable = false;
		}
	}
}
