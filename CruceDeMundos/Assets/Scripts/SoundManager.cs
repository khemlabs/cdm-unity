﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
	public AudioSource soundFX;
	public AudioSource music;

	public static SoundManager instance = null;

	public float lowPitch = 0.95f;
	public float highPitch = 1.05f;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			if (instance != this) Destroy(gameObject);
		}
	}

	// Update is called once per frame
	void Update()
	{

	}
}
