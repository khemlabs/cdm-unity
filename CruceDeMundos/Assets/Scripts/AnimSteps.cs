﻿using UnityEngine;
using System.Collections;

public class AnimSteps : MonoBehaviour {

	public GameObject fade;
	public int step;
	private Animation anim;

	public GameObject toDestroy;

	private AudioSource source;

	void Start () {
		source = gameObject.GetComponent<AudioSource> ();
		anim = GetComponent<Animation> ();
	}
	
	public void SetNextStep(int nextStep){
		step = nextStep;
		anim.Play ();
	}

	public void CheckStep(int step2Check){	
		if (step<step2Check)
			anim.Play ();
		else
			anim.Stop();
	}

	public void DestroyGO(){
		toDestroy.GetComponent<CircleCollider2D> ().enabled = false;
		FadeOut (4f);
		Destroy (toDestroy, 5);
	}

	public void FadeOut(float seconds){
		GameObject f = Instantiate (fade);
		f.transform.parent = Data.Instance.gameObject.transform;
		Fade fadeOut = f.GetComponent<Fade> ();

		fadeOut.OnBeginMethod = () => {			
		};
		fadeOut.OnLoopMethod = () => {
			float vol = Mathf.Lerp (0, 1, fadeOut.time);
			if(source!=null)
				source.volume = vol;
		};
		fadeOut.OnEndMethod = () => {
			if(source!=null){
				source.Stop();
				source.volume = 1f;
			}
			fadeOut.Destroy();
		};
		fadeOut.StartFadeOut (seconds);
	}
}
