﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvalidUser : MonoBehaviour
{
    public GameObject invalidUser;

    private void Start()
    {
        invalidUser.SetActive(false);
    }

    public void Ok()
    {
        invalidUser.SetActive(false);
    }
}
