﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalProgress : MonoBehaviour {

    public Text portals;

	// Use this for initialization
	void Start () {
        //En caso que el GameManager no sea null.
        if (Game.Instance.gameManager != null)
        {
            //Inicializo la cantidad de portales que se muestra en pantalla.
            portals.text = Game.Instance.gameManager.portals.ToString();
        }
        else
        {
            //Caso contrario lo pongo en "cero"
            //tras el primer update va a tener el verdadero valor.
            portals.text = "0";
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Actualizo la cantidad de portales que se muestra en pantalla.
        portals.text = Game.Instance.gameManager.portals.ToString();
    }
}
