﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ToolsManager : MonoBehaviour
{

	public FriendTool[] friendsTools;
	public List<string> damagingObstacles;
	public GameObject btnNextTools;
	int ToolsSelected;
	int activeFriends;

	[System.Serializable]
	public class FriendTool
	{
		public string name;
		public GameObject friend;
		public string toolName;
		public int toolLevel;
		public bool hasCharge;
	}

	// Use this for initialization
	void Start()
	{
		Events.OnChargeCollect += OnChargeCollect;
		Events.OnAddTool += OnAddTool;
	}

	void OnDestroy()
	{
		Events.OnChargeCollect -= OnChargeCollect;
		Events.OnAddTool -= OnAddTool;
	}

	void OnChargeCollect(int val, PlayerData.ToolName tType)
	{
		int cant = Array.FindAll(friendsTools, p => p.toolName == tType.ToString()).Length;
		float charge = 1f * val / cant;
		for (int i = 0; i < friendsTools.Length; i++)
		{
			if (friendsTools[i].toolName == tType.ToString())
			{
				friendsTools[i].hasCharge = true;
				HealthBar healthBar = friendsTools[i].friend.GetComponentInChildren<HealthBar>();
				//healthBar.CurrentHealth = Mathf.Min(healthBar.currentHealth+charge, 100);
				healthBar.SetHealth(Mathf.Min(healthBar.currentHealth + charge, 100), 3f);
			}
		}

		CheckRemoveDamagingObstacle(tType.ToString());
	}

	void OnAddTool(PlayerData.ToolName tType)
	{
		FriendTool friendTool = Array.Find(friendsTools, p => p.toolName == "" && p.friend.activeSelf);

		if (friendTool == null)
		{
			friendTool = friendsTools[0];
			Transform t = friendTool.friend.transform.Find(friendTool.toolName);
			ReplaceDuplicated();
			t.gameObject.SetActive(false);
		}

		Transform tool = friendTool.friend.transform.Find(tType.ToString());
		if (!tool.gameObject.activeSelf)
		{
			tool.gameObject.SetActive(true);
			GameObject healthBar = friendTool.friend.transform.Find("HealthBar").gameObject;
			healthBar.SetActive(true);
			if (tType.ToString().Equals(PlayerData.ToolName.Restaurador.ToString()))
			{
				healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().portalEnergy;
			}
			else if (tType.ToString().Equals(PlayerData.ToolName.Matafuegos.ToString()))
			{
				healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().fireEnergy;
			}
			else if (tType.ToString().Equals(PlayerData.ToolName.Armonizador.ToString()))
			{
				healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().pollutionEnergy;
			}

			friendTool.friend.transform.Find("HealthBarBackground").gameObject.SetActive(true);
			SetTool(friendTool, tType.ToString(), 0);
			ToolsSelected++;
			Data.Instance.playerData.toolsNumber++;
		}
	}

	/// <summary>
	/// Reemplaza la herramienta que este duplicada en los amigos por la que tiene el
	/// jugador en el primer amigo.
	/// </summary>
	void ReplaceDuplicated()
	{
		int activePlayers = 0;

		foreach (var friendTool in friendsTools)
		{
			Debug.Log("Jugador Activo " + friendTool.name + " ? --->" + friendTool.friend.activeSelf);
			if (friendTool.friend.activeSelf) activePlayers++;
		}
		Debug.Log("Jugadores Activos = " + activePlayers);

		if (activePlayers > 2)
		{
			//Tenemos herramientas duplicadas distintas a la del jugador principal
			if (friendsTools[0].toolName != friendsTools[1].toolName && friendsTools[0].toolName != friendsTools[2].toolName && friendsTools[1].toolName == friendsTools[2].toolName)
			{
				friendsTools[1].friend.transform.Find(friendsTools[1].toolName).gameObject.SetActive(false);

				Transform tool = friendsTools[1].friend.transform.Find(friendsTools[0].toolName);
				if (!tool.gameObject.activeSelf)
				{
					tool.gameObject.SetActive(true);
					GameObject healthBar = friendsTools[1].friend.transform.Find("HealthBar").gameObject;
					healthBar.SetActive(true);
					if (friendsTools[0].toolName.Equals(PlayerData.ToolName.Restaurador.ToString()))
					{
						healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().portalEnergy;
					}
					else if (friendsTools[0].toolName.ToString().Equals(PlayerData.ToolName.Matafuegos.ToString()))
					{
						healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().fireEnergy;
					}
					else if (friendsTools[0].toolName.ToString().Equals(PlayerData.ToolName.Armonizador.ToString()))
					{
						healthBar.GetComponent<SpriteRenderer>().sprite = healthBar.GetComponent<HealthBar>().pollutionEnergy;
					}

					friendsTools[1].friend.transform.Find("HealthBarBackground").gameObject.SetActive(true);
					SetTool(friendsTools[1], friendsTools[0].toolName.ToString(), 0);
					ToolsSelected++;
					Data.Instance.playerData.toolsNumber++;
				}
			}
		}
	}

	public void SetFriendTool(GameObject friend, string toolName, int tLevel)
	{
		FriendTool ft = Array.Find(Game.Instance.toolsManager.friendsTools, p => p.friend == friend);
		SetTool(ft, toolName, tLevel);
	}

	void SetTool(FriendTool ft, string toolName, int tLevel)
	{
		ft.toolName = toolName;
		ft.hasCharge = true;
		ft.toolLevel = tLevel;

		CheckRemoveDamagingObstacle(ft.toolName);
	}

	public void SetFriendEmpty(GameObject friend, bool remove)
	{
		FriendTool ft = Array.Find(friendsTools, p => p.friend == friend);
		ft.hasCharge = false;

		FriendTool f = Array.Find(friendsTools, p => p.toolName == ft.toolName && p.hasCharge == true);

		if (ft.toolName.Equals(PlayerData.ToolName.Matafuegos.ToString()) && f == null)
			damagingObstacles.Add(ShootObstacle.ObstacleType.FIRE.ToString());
		else if (ft.toolName.Equals(PlayerData.ToolName.Restaurador.ToString()) && f == null)
			damagingObstacles.Add(ShootObstacle.ObstacleType.PORTAL.ToString());
		else if (ft.toolName.Equals(PlayerData.ToolName.Armonizador.ToString()) && f == null)
			damagingObstacles.Add(ShootObstacle.ObstacleType.POLLUTION.ToString());

		if (remove)
			ft.toolName = "";
	}

	void CheckRemoveDamagingObstacle(string tType)
	{
		if (tType.Equals(PlayerData.ToolName.Matafuegos.ToString()) && damagingObstacles.Contains(ShootObstacle.ObstacleType.FIRE.ToString()))
			damagingObstacles.Remove(ShootObstacle.ObstacleType.FIRE.ToString());
		else if (tType.Equals(PlayerData.ToolName.Restaurador.ToString()) && damagingObstacles.Contains(ShootObstacle.ObstacleType.PORTAL.ToString()))
			damagingObstacles.Remove(ShootObstacle.ObstacleType.PORTAL.ToString());
		else if (tType.Equals(PlayerData.ToolName.Armonizador.ToString()) && damagingObstacles.Contains(ShootObstacle.ObstacleType.POLLUTION.ToString()))
			damagingObstacles.Remove(ShootObstacle.ObstacleType.POLLUTION.ToString());
	}

	public bool CanDamage(string tag)
	{
		if (damagingObstacles.Count > 0)
		{
			return damagingObstacles.Contains(tag);
		}
		else
		{
			return false;
		}
	}

	public void SelectedToolsData()
	{
		string result = "";
		int j = 0;

		for (int i = 0; i < friendsTools.Length; i++)
		{
			if (friendsTools[i].toolName != "")
			{
				if (j != 0)
					result += ";";
				result += friendsTools[i].name + ":" + friendsTools[i].toolName + "_" + friendsTools[i].toolLevel;
				j++;
				ToolsSelected = j;
			}
		}

		Game.Instance.levelMetrics.tools = result;
	}

	public void FinalToolsCharge()
	{
		string result = "";
		int j = 0;
		for (int i = 0; i < friendsTools.Length; i++)
		{
			if (friendsTools[i].toolName != "")
			{
				HealthBar hb = friendsTools[i].friend.transform.GetComponentInChildren<HealthBar>();
				if (j != 0)
					result += ";";
				result += friendsTools[i].name + ":" + hb.currentHealth + "%";
				j++;
			}
		}
		Game.Instance.levelMetrics.toolsEndCharge = result;
	}

	public void DisableFriend(string fn)
	{
		FriendTool ft = Array.Find(friendsTools, p => p.name == fn);
		if (ft != null)
		{
			SetFriendEmpty(ft.friend, false);
			ft.friend.SetActive(false);
		}
	}

	private void Update()
	{
		//Validamos cuantas herramientas se seleccionaron
		//y cuantos amigos activos hay
		if (ToolsSelected <= 3)
		{
			for (int i = 0; i < friendsTools.Length; i++)
			{
				if (friendsTools[i].hasCharge) ToolsSelected++;
				if (friendsTools[i].friend.activeSelf) activeFriends++;
			}

		}

		//En el caso de que coincida la cantidad de amigos activos con la cantidad de herramientas seleccionadas,
		//hacemos el boton de siguiente inteccionable.
		btnNextTools.GetComponent<Button>().interactable = (ToolsSelected >= 1 || Game.Instance.gameManager.resources < 50);

		ToolsSelected = 0;
		activeFriends = 0;
	}
}
