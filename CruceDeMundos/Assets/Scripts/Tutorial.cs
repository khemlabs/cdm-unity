﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour {

	public GameObject[] step;
    public GameObject[] reminderPortal;
    public GameObject[] reminderFire;
    public float tutoVel;
    List<Vector3> positionsTutorial;
    List<Vector3> positionsReminder;
	public int indexTutorial = 0;
    public int indexReminder = 0;
    public int indexPortal = 0;
    public int indexFire = 0;
    public Transform PlayerTransform;
    public bool showingTutorial;
    public bool showingReminder;
    readonly int radio = 5;
    public List<GameObject> players;
    public List<ShootObstacle> shootObstaclesRestorers;
    public List<ShootObstacle> shootObstaclesFireExtinguishers;

    // Use this for initialization
    void Start () {
        //Posiciones de cada region para activar los hints del tutorial.
        positionsTutorial = new List<Vector3>
        {
            new Vector3(0, 0, 0),
            new Vector3(15, -5, 0),
            new Vector3(15, 6.5f, 0),
            new Vector3(15, 15, 0),
            new Vector3(30, -25, 0),
            new Vector3(50, -25, 0),
            new Vector3(65, 0, 0)
        };
        //Posiciones de los reminders del nivel tutorial
        positionsReminder = new List<Vector3>
        {
            new Vector3(15, 12.5f, 0),
            new Vector3(50, -20f, 0),
            new Vector3(70, -20, 0)
        };
        showingTutorial = true;
        showingReminder = false;
    }
	
	// Update is called once per frame
	void Update () {
        //Para salir de tutorial mientras se muestra 
        if (Input.GetKeyDown(KeyCode.Z) && showingTutorial)
        {
            //desactiva el último tutorial que se mostro
            step[indexTutorial - 1].SetActive(false);
            showingTutorial = false;
            Game.Instance.OnGamePaused(false);
        }

        //Para salir de reminder mientras se muestra 
        if (Input.GetKeyDown(KeyCode.Z) && showingReminder)
        {
            //desactiva el último reminder que se mostro
            if (indexPortal > 0 && reminderPortal[indexPortal - 1].activeInHierarchy) reminderPortal[indexPortal - 1].SetActive(false);

            if (indexFire > 0 && reminderFire[indexFire - 1].activeInHierarchy) reminderFire[indexFire - 1].SetActive(false);

            showingReminder = false;
            Game.Instance.OnGamePaused(false);
        }

        //Si el jugador esta dentro de un radio cercano a una de las posiciones, habilita el tutorial de esa region
        if (indexTutorial < step.Length)
        {
            if (indexTutorial != 0 && Vector3.Distance(PlayerTransform.position, positionsTutorial[indexTutorial]) < radio)
            {
                //Activa el tutorial correspondiente
                step[indexTutorial].SetActive(true);
                showingTutorial = true;
                indexTutorial++;
            }
        }

        //ANTIPORTALES
        //si el jugador esta dentro del radio de las posiciones, se habilita el recordatorio para completar una accion.
        if (indexPortal < reminderPortal.Length)
        {
            //Buscamos entre los restauradores que tienen el jugador y sus amigos cual esta en distancia a los portales
            foreach (ShootObstacle restorer in shootObstaclesRestorers)
            {
                var findRestorer = restorer.obstaclesInRange.Find(portal => portal.tag == restorer.oType.ToString());
                if (findRestorer != null && findRestorer.tag == "PORTAL" && Vector3.Distance(PlayerTransform.position, positionsReminder[indexReminder]) < radio)
                {
                    reminderPortal[indexPortal].SetActive(true);
                    showingReminder = true;
                    indexReminder++;
                    indexPortal++;
                }
            }

        }

        //EXTINTORES
        //si el jugador esta dentro del radio de las posiciones, se habilita el recordatorio para completar una accion.
        if (indexFire < reminderFire.Length)
        {
            //Buscamos entre los extintores que tienen el jugador y sus amigos cual esta en distancia a los incendios
            foreach (ShootObstacle fireExinguisher in shootObstaclesFireExtinguishers)
            {
                if (fireExinguisher != null)
                {
                    var findFireExinguisher = fireExinguisher.obstaclesInRange.Find(portal => portal.tag == fireExinguisher.oType.ToString());
                    if (findFireExinguisher != null && findFireExinguisher.tag == "FIRE" && Vector3.Distance(PlayerTransform.position, positionsReminder[indexReminder]) < radio)
                    {
                        reminderFire[indexFire].SetActive(true);
                        showingReminder = true;
                        indexReminder++;
                        indexFire++;
                    }
                }
            }
        }

        //Detiene el tiempo de juego
        if (showingReminder || showingTutorial)
        {
            Game.Instance.OnGamePaused(true);
        }

    }

	void OnEnable(){
        //Hacemos Start para cargar las posiciones relativas a cada region de tutorial
        Start();

        //Si el jugador esta dentro de un radio cercano a una de las posiciones, habilita el tutorial de esa region
        if (Vector3.Distance(PlayerTransform.position, positionsTutorial.ToArray()[indexTutorial]) < radio)
        {
            //Activa el tutorial correspondiente
            step[indexTutorial].SetActive(true);
            showingTutorial = true;
            indexTutorial++;
        }

        //ANTIPORTALES
        //si el jugador esta dentro del radio de las posiciones, se habilita el recordatorio para completar una accion.
        if (indexPortal < reminderPortal.Length)
        {
            //Buscamos entre los restauradores que tienen el jugador y sus amigos cual esta en distancia a los portales
            foreach (ShootObstacle restorer in shootObstaclesRestorers)
            {
                var findRestorer = restorer.obstaclesInRange.Find(portal => portal.tag == restorer.oType.ToString());
                if (findRestorer != null && findRestorer.tag == "PORTAL" && Vector3.Distance(PlayerTransform.position, positionsReminder[indexReminder]) < radio)
                {
                    reminderPortal[indexPortal].SetActive(true);
                    showingReminder = true;
                    indexReminder++;
                    indexPortal++;
                }
            }

        }

        //EXTINTORES
        //si el jugador esta dentro del radio de las posiciones, se habilita el recordatorio para completar una accion.
        if (indexFire < reminderFire.Length)
        {
            //Buscamos entre los extintores que tienen el jugador y sus amigos cual esta en distancia a los incendios
            foreach (ShootObstacle fireExinguisher in shootObstaclesFireExtinguishers)
            {
                if (fireExinguisher != null)
                {
                    var findFireExinguisher = fireExinguisher.obstaclesInRange.Find(portal => portal.tag == fireExinguisher.oType.ToString());
                    if (findFireExinguisher != null && findFireExinguisher.tag == "FIRE" && Vector3.Distance(PlayerTransform.position, positionsReminder[indexReminder]) < radio)
                    {
                        reminderFire[indexFire].SetActive(true);
                        showingReminder = true;
                        indexReminder++;
                        indexFire++;
                    }
                }
            }
        }

        if (showingReminder || showingTutorial)
        {
            Game.Instance.OnGamePaused(true);
        }
    }
}
