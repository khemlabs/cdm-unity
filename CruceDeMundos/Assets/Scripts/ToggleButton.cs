﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleButton : MonoBehaviour
{

	public GameObject buttonOn;
	public GameObject buttonOff;
	public bool defaultValue;

	// Use this for initialization
	void Awake()
	{
		SetButtonOn(defaultValue);
	}

	public void SetButtonOn(bool on)
	{
		buttonOff.SetActive(!on);
		buttonOn.SetActive(on);
	}
}
