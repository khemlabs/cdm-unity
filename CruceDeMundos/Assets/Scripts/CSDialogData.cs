﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using SimpleJSON;
using System.IO;
using UnityEngine.Networking;
//using System.Web;

public class CSDialogData : MonoBehaviour
{

	public bool sendDialogs2Database;
	public DialogCharacter[] dialogCharacters;
	public List<Dialog> dialogs;
	public string PATH = "/Resources/Dialogs";

	void Start()
	{
		dialogs = new List<Dialog>();

#if UNITY_WEBGL && !UNITY_EDITOR
		string URL = Data.Instance.baseURL;
		List<string> dialogsNames = dialogsList();
		foreach (String filename in dialogsNames)
		{
			StartCoroutine(Import(URL + "/CsDialogs/" + filename));
		}
#else

#if UNITY_STANDALONE_OSX && !UNITY_EDITOR
		DirectoryInfo dir = new DirectoryInfo(Directory.GetParent(Directory.GetParent(Application.dataPath).FullName).FullName+PATH);
#else
		DirectoryInfo dir = new DirectoryInfo(Directory.GetParent(Application.dataPath).FullName + PATH);
#endif
		FileInfo[] info = dir.GetFiles("*.json");

		foreach (FileInfo f in info)
		{
			if (!f.FullName.Contains("fulldialogs"))
			{
				StartCoroutine(Import(f.FullName));
			}
		}

#endif
	}

	List<string> dialogsList()
	{
		List<string> dialogsNames = new List<string>();
		dialogsNames.Add("DraGrimberg_CS_level_4.json");
		dialogsNames.Add("Manu_CS_Level_4.json");
		return dialogsNames;
	}

	[Serializable]
	public class DialogCharacter
	{
		public string name;
		public GameObject visualization;
		public int lastEmoVal;
		public int globalEmoVal;
		public List<LevelInfo> levelsInfo;

		[Serializable]
		public class LevelInfo
		{
			public int level;
			public int emoval;
			public int goTo;
			public string lastExpre;
			public Dialog.DType dtype;
		}

		public void ResetLevel(int level)
		{
			LevelInfo l = levelsInfo.Find(x => x.level == level);
			if (l != null)
			{
				l.emoval = 0;
				l.goTo = 0;
			}
		}

		public void ResetType(int level, Dialog.DType dtype)
		{
			LevelInfo l = levelsInfo.Find(x => x.level == level && x.dtype == dtype);
			if (l != null)
			{
				l.emoval = 0;
				l.goTo = 0;
			}
		}
	}

	public void ResetAllAtLevel(int level)
	{
		foreach (DialogCharacter dch in dialogCharacters)
			dch.ResetLevel(level);
	}

	public void ResetHintAtLevel(int level)
	{
		foreach (DialogCharacter dch in dialogCharacters)
		{
			dch.ResetType(level, Dialog.DType.LEVEL);
		}
	}

	IEnumerator Import(string file)
	{
		string text = "";
		//Para buil de WebGL descomentar la siguiente linea
		//UnityWebRequest www = UnityWebRequest.Get(file);
		//y comentar la siguiente
		UnityWebRequest www = UnityWebRequest.Get("file://" + file);

		yield return www.SendWebRequest();

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			text = www.downloadHandler.text;
		}

		var N = JSON.Parse(text);

		Dialog d = new Dialog
		{
			name = N["name"],
			level = N["level"].AsInt,
			dialogType = CastDialogType(N["type"]),
			initial = N["initial"] != null ? N["initial"].AsBool : false,
			final = N["final"] != null ? N["final"].AsBool : false,
			dialogTree = new Dialog.DialogTree[N["dialogTree"].Count]
		};
		for (int i = 0; i < d.dialogTree.Length; i++)
		{
			d.dialogTree[i] = new Dialog.DialogTree
			{
				index = N["dialogTree"][i]["index"].AsInt,
				moods = new Dialog.Mood[N["dialogTree"][i]["moods"].Count]
			};
			for (int j = 0; j < d.dialogTree[i].moods.Length; j++)
			{
				d.dialogTree[i].moods[j] = new Dialog.Mood
				{
					mType = CastMoodType(N["dialogTree"][i]["moods"][j]["mood"]),
					prompt = N["dialogTree"][i]["moods"][j]["prompt"]
				};
				if (N["dialogTree"][i]["moods"][j]["expre"] != null)
					d.dialogTree[i].moods[j].expre = N["dialogTree"][i]["moods"][j]["expre"];
				else
					d.dialogTree[i].moods[j].expre = N["dialogTree"][i]["moods"][j]["expre"] = "";
				d.dialogTree[i].moods[j].background = N["dialogTree"][i]["moods"][j]["background"];
				d.dialogTree[i].moods[j].indicador = N["dialogTree"][i]["moods"][j]["indicador"] != null ? N["dialogTree"][i]["moods"][j]["indicador"].AsBool : false;
				d.dialogTree[i].moods[j].replies = new Dialog.Reply[N["dialogTree"][i]["moods"][j]["replies"].Count];
				for (int k = 0; k < d.dialogTree[i].moods[j].replies.Length; k++)
				{
					d.dialogTree[i].moods[j].replies[k] = new Dialog.Reply
					{
						emoVal = N["dialogTree"][i]["moods"][j]["replies"][k]["emoVal"].AsInt,
						exit = N["dialogTree"][i]["moods"][j]["replies"][k]["exit"].AsBool,
						goTo = N["dialogTree"][i]["moods"][j]["replies"][k]["goTo"].AsInt,
						text = N["dialogTree"][i]["moods"][j]["replies"][k]["text"]
					};

					string rt = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["rType"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].replyType = CastReplyType(N["dialogTree"][i]["moods"][j]["replies"][k]["rType"]);
						rt = N["dialogTree"][i]["moods"][j]["replies"][k]["rType"];
						rt = rt.Replace(",", ";\n");
					}
					/*else
					d.dialogTree [i].moods [j].replies [k].replyType = Dialog.Reply.rType.NARRATIVO;*/

					string rst = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].replySubType = CastReplySubType(N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"]);
						rst = N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"];
						rst = rst.Replace(",", ";\n");
					}
					/*else
					d.dialogTree [i].moods [j].replies [k].replySubType = Dialog.Reply.rSubType.NARRATIVO;*/

					string iv = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["indicVal"] != null)
					{
						iv = N["dialogTree"][i]["moods"][j]["replies"][k]["indicVal"];
						d.dialogTree[i].moods[j].replies[k].indicadorVal = iv.Split(',');
						iv = iv.Replace(",", ";\n");
					}/*else
		d.dialogTree [i].moods [j].replies [k].indicadorVal ="";*/

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["resources"] != null)
						d.dialogTree[i].moods[j].replies[k].resources = N["dialogTree"][i]["moods"][j]["replies"][k]["resources"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].resources = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["fireCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].fireCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["fireCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].fireCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["portalCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].portalCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["portalCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].portalCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["pollutionCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].pollutionCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["pollutionCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].pollutionCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["tool"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].tool = N["dialogTree"][i]["moods"][j]["replies"][k]["tool"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].tool = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["objective"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].objective = N["dialogTree"][i]["moods"][j]["replies"][k]["objective"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].objective = false;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["dialog"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].dialog = N["dialogTree"][i]["moods"][j]["replies"][k]["dialog"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].dialog = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["levelEnd"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].levelEndDialog = N["dialogTree"][i]["moods"][j]["replies"][k]["levelEnd"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].levelEndDialog = false;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["move"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].move = N["dialogTree"][i]["moods"][j]["replies"][k]["move"].AsInt;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].move = -1;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["block"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].block = N["dialogTree"][i]["moods"][j]["replies"][k]["block"].AsInt;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].block = 0;
					}

					LevelData.DialogUnlock du = new LevelData.DialogUnlock();
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["unlock"] != null)
					{
						string s = N["dialogTree"][i]["moods"][j]["replies"][k]["unlock"];
						string[] ss = s.Split(',');
						du.characterName = ss[0];
						du.goTo = int.Parse(ss[1]);
						d.dialogTree[i].moods[j].replies[k].dUnlock = du;
					}
					else
					{
						du.characterName = "";
						du.goTo = -1;
						d.dialogTree[i].moods[j].replies[k].dUnlock = du;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["obstacle"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].oType = N["dialogTree"][i]["moods"][j]["replies"][k]["obstacle"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].oType = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["friendDisable"] != null)
						d.dialogTree[i].moods[j].replies[k].friendDisable = N["dialogTree"][i]["moods"][j]["replies"][k]["friendDisable"];
					else
						d.dialogTree[i].moods[j].replies[k].friendDisable = "";

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["blackout"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].blackout = N["dialogTree"][i]["moods"][j]["replies"][k]["blackout"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].blackout = false;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["levelMap"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].levelMap = N["dialogTree"][i]["moods"][j]["replies"][k]["levelMap"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].levelMap = false;
					}

					if (sendDialogs2Database)
					{
						StartCoroutine(Data.Instance.dataController.AddDialog(d.name, d.level, d.dialogTree[i].index,
						d.dialogType.ToString(), d.dialogTree[i].moods[j].mType.ToString(), d.dialogTree[i].moods[j].prompt,
						k, d.dialogTree[i].moods[j].replies[k].text, rt, rst, iv));
					}
				}
			}
		}
		dialogs.Add(d);
	}

	[Serializable]
	public class Dialog
	{
		public enum DType
		{
			NARRATIVE,
			LEVEL
		}
		public string name;
		public int level;
		public DType dialogType;
		public bool initial;
		public bool final;
		public DialogTree[] dialogTree;

		[Serializable]
		public class DialogTree
		{
			public int index;
			public Mood[] moods;

		}
		[Serializable]
		public class Mood
		{
			public enum MoodType
			{
				NEGATIVE,
				NEUTRAL,
				POSITIVE
			}
			public MoodType mType;
			public string expre;
			public string background;
			public bool indicador;
			public string prompt;
			public Reply[] replies;

		}
		[Serializable]
		public class Reply
		{
			public int emoVal;
			public bool exit;
			public int goTo;
			public string text;
			public List<RType> replyType;
			public List<RSubType> replySubType;
			public string[] indicadorVal;
			public int resources;
			public int fireCharge;
			public int portalCharge;
			public int pollutionCharge;
			public string tool;
			public bool objective;
			public string dialog;
			public bool levelEndDialog;
			public int move;
			public int block;
			public LevelData.DialogUnlock dUnlock;
			public string oType;
			public string friendDisable;
			public bool blackout;
			public bool levelMap;

			public enum RType
			{
				NARRATIVO,

				ASERTIVIDAD,
				AUTOEFICACIA,
				COLABORATIVO,
				EMPATÍA
			}

			public enum RSubType
			{
				NARRATIVO,

				//asertividad
				PROACTIVIDAD,
				INTERÉS,
				PEDIDO,
				NEGARSE,

				//autoeficacia
				JUICIO,
				AUTOPERCEPCIÓN,

				//colaborativo
				COMPARTIR,
				ACEPTAR,
				CONFIANZA,
				TODOS,

				//empatía
				RECONOCIMIENTO,
				ACCIÓN
			}
		}

	}

	Dialog.Mood.MoodType CastMoodType(string s)
	{
		return (Dialog.Mood.MoodType)System.Enum.Parse(typeof(Dialog.Mood.MoodType), s.ToUpperInvariant());
	}

	Dialog.DType CastDialogType(string s)
	{
		return (Dialog.DType)System.Enum.Parse(typeof(Dialog.DType), s.ToUpperInvariant());
	}


	List<Dialog.Reply.RType> CastReplyType(string s)
	{
		List<Dialog.Reply.RType> l = new List<Dialog.Reply.RType>();
		string[] sl = s.Split(',');
		foreach (string st in sl)
			l.Add((Dialog.Reply.RType)System.Enum.Parse(typeof(Dialog.Reply.RType), st.ToUpperInvariant()));
		return l;
	}

	List<Dialog.Reply.RSubType> CastReplySubType(string s)
	{
		List<Dialog.Reply.RSubType> l = new List<Dialog.Reply.RSubType>();
		string[] sl = s.Split(',');
		foreach (string st in sl)
			l.Add((Dialog.Reply.RSubType)System.Enum.Parse(typeof(Dialog.Reply.RSubType), st.ToUpperInvariant()));
		return l;
	}

	public string GetDialogData()
	{
		string json = "dialogData:[\n";
		for (int i = 0; i < dialogCharacters.Length; i++)
		{
			json += "{\n";
			json += "name:" + dialogCharacters[i].name + ",\n";
			json += "lastEmoVal:" + dialogCharacters[i].lastEmoVal + ",\n";
			json += "globalEmoVal:" + dialogCharacters[i].globalEmoVal + ",\n";
			json += "levelsInfo:[";
			for (int j = 0; j < dialogCharacters[i].levelsInfo.Count; j++)
			{
				json += "{";
				json += "level:" + dialogCharacters[i].levelsInfo[j].level + ",";
				json += "emoval:" + dialogCharacters[i].levelsInfo[j].emoval + ",";
				json += "goTo:" + dialogCharacters[i].levelsInfo[j].goTo + ",";
				json += "lastExpre:" + dialogCharacters[i].levelsInfo[j].lastExpre + ",";
				json += "dtype:" + dialogCharacters[i].levelsInfo[j].dtype;
				json += "}";
				if (j < dialogCharacters[i].levelsInfo.Count - 1)
					json += ",";
			}
			json += "]\n}";
			if (i < dialogCharacters.Length - 1)
				json += ",\n";
		}
		json += "]\n";

		return json;
	}

	public void SetDialogData(JSONNode N)
	{
		for (int i = 0; i < N.Count; i++)
		{
			dialogCharacters[i].lastEmoVal = N[i]["lastEmoVal"].AsInt;
			dialogCharacters[i].globalEmoVal = N[i]["globalEmoVal"].AsInt;
			dialogCharacters[i].levelsInfo.Clear();
			for (int j = 0; j < N[i]["levelsInfo"].Count; j++)
			{
				DialogCharacter.LevelInfo li = new DialogCharacter.LevelInfo
				{
					level = N[i]["levelsInfo"][j]["level"].AsInt,
					emoval = N[i]["levelsInfo"][j]["emoval"].AsInt,
					goTo = N[i]["levelsInfo"][j]["goTo"].AsInt,
					lastExpre = N[i]["levelsInfo"][j]["lastExpre"],
					dtype = (Dialog.DType)System.Enum.Parse(typeof(Dialog.DType), N[i]["levelsInfo"][j]["dtype"])
				};
				dialogCharacters[i].levelsInfo.Add(li);
			}
		}
	}
}
