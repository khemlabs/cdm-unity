﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
///     Esto anima un dialogo y hace que aparezca letra por letra
/// </summary>
public class DialogAnimation : MonoBehaviour {
    public string cadena;
    public float velocidad = 0.005f;
    private string cadenaActual = ""; 

    private bool estaReproduciendo = false;

	// Use this for initialization
	public void Start () {
        if(this.GetComponent<Text>().text != null)
        {
            cadena = this.GetComponent<Text>().text;
        }
        else
        {
            cadena = "";
        }

        StartCoroutine(ShowText());
    }

    // Update is called once per frame
    void Update () {
        //Completar el dialogo
        if (Input.GetKeyDown(KeyCode.Space) && estaReproduciendo)
        {
            StopAllCoroutines();
            this.GetComponent<Text>().text = cadena;
            estaReproduciendo = false;
        }

        //Cargar un nuevo dialogo para ser recorrido.
        if(cadena != this.GetComponent<Text>().text && !estaReproduciendo)
        {
            StopAllCoroutines();
            cadena = this.GetComponent<Text>().text;
            StartCoroutine(ShowText());
        }
    }

    //Rutina que se encarga de recorrer el texto original letra por letra
    private IEnumerator ShowText()
    {
        WaitForSeconds retraso = new WaitForSeconds(velocidad);
        //Recorremos la cadena cargada desde el dialogo
        for (int i = 0; i <= cadena.Length; i++)
        {
            estaReproduciendo = true;
            //asignamos a la cadena actual la subcadena de 0 a la posicion actual del ciclo
            cadenaActual = cadena.Substring(0, i);
            //pasamos esta cadena al elemento texto del dialogo
            this.GetComponent<Text>().text = cadenaActual;
            //Se hace una espera de tantos segundos
            yield return retraso;
        }
        estaReproduciendo = false;
    }
}
