﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Security;
using System;
using System.Threading.Tasks;

/// <summary>
///  Servicio de validación de usuario con la base del servidor.
/// </summary>
public class UserServices : MonoBehaviour
{
	public string user;
	public Button play;
	public GameObject InvalidUser;
	public GameObject networkMessages;
	public bool UserDone, userOk, sessionExist;
	public bool LoadDone { get; set; }

	void Awake()
	{
		Data.Instance.Mute(false);
		networkMessages.SetActive(false);
	}

	public void SetUser(string newUser)
	{
		if (newUser != "" && newUser.Length == 6)
		{
			user = newUser;
			UserDone = true;
			SetPlay();
		}
		else
		{
			UserDone = false;
		}
	}

	void SetPlay()
	{
		Data.Instance.userId = user.ToUpper();
		Data.Instance.dataController.UserID = user.ToUpper();
		Data.Instance.avatarData.UserID = user.ToUpper();

		play.interactable = UserDone;
	}

	public void Play()
	{
		StartCoroutine(CheckUser());
	}

	public async void EnterGameWeb()
	{
		//Acá valido con la DB si el usuario existe caso contrario no lo dejo ingresar.
		if (userOk)
		{
			//Verifico que el usuario tiene sesion de juego o no
			StartCoroutine(CheckSession());

			//Data.Instance.avatarData.LoadAvatarData(user.ToUpper());
			Data.Instance.gameTimeInit = Time.realtimeSinceStartup;

			if (userOk && sessionExist)
			{
				Data.Instance.userId = user;
				await Data.Instance.LoadUserDataWeb(user);
				await Data.Instance.LoadWebGameData(user);
				await Data.Instance.avatarData.LoadAvatarDataWeb(user);

				SceneManager.LoadScene("LevelMap");
			}
			else
			{
				//Por seguridad se borran los registros de player prefs del registro de Windows.

				//Creamos los archivos o registros del usuario
				//Datos del usuario

				Data.Instance.userId = user;

				SceneManager.LoadScene("Sign");
			}
		}
		else
		{
			//En caso que no este dentro de la DB pongo el boton de play como no intereccionable.
			//Además limpio el campo user , para que intente nuevamente. Además muestro un mensaje
			// de usuario invalido.
			play.interactable = false;
			user = "";
		}
	}

	public void EnterGame()
	{
		//Acá valido con la DB si el usuario existe caso contrario no lo dejo ingresar.
		if (userOk)
		{
			//Verifico que el usuario tiene sesion de juego o no
			StartCoroutine(CheckSession());

			//Data.Instance.avatarData.LoadAvatarData(user.ToUpper());
			Data.Instance.gameTimeInit = Time.realtimeSinceStartup;

			if (userOk && sessionExist)
			{
				Data.Instance.userId = user;
				Data.Instance.LoadUserData(user);
				Data.Instance.LoadGameData(user);
				Data.Instance.avatarData.LoadAvatarData(user);

				SceneManager.LoadScene("LevelMap");
			}
			else
			{
				//Por seguridad se borran los registros de player prefs del registro de Windows.

				//Creamos los archivos o registros del usuario
				//Datos del usuario

				Data.Instance.userId = user;

				SceneManager.LoadScene("Sign");
			}
		}
		else
		{
			//En caso que no este dentro de la DB pongo el boton de play como no intereccionable.
			//Además limpio el campo user , para que intente nuevamente. Además muestro un mensaje
			// de usuario invalido.
			play.interactable = false;
			user = "";
		}
	}

	IEnumerator CheckUser()
	{
		CoroutineWithResponse response = new CoroutineWithResponse(this, Data.Instance.dataController.CheckUser(user));

		yield return response.coroutine;
		if (response.result.ToString() != "")
		{
			if (response.result.ToString() == "Conectando con el servidor...")
			{
				userOk = true;
				if (Data.Instance.SyncServerMode)
				{
					EnterGameWeb();
				}
				else
				{
					EnterGame();
				}

			}
			networkMessages.SetActive(true);
			networkMessages.GetComponent<NetworkMessages>().setMessage(response.result.ToString());
		}
	}

	IEnumerator CheckSession()
	{
		string userData = "";

		//Se intenta cargar los datos del usuario
		userData = new SaverUtil().loadData(user.ToUpper() + "userData");

		if (!userData.Equals(""))
		{
			sessionExist = true;
			//Rutina de verificación de sesión web vs local
			//StartCoroutine(Data.Instance.dataController.VerifyLastSession(gameData));
		}
		else
		{
			sessionExist = false;
		}

		yield return userData != "";
	}

	IEnumerator AsynchronousLoad(string scene)
	{
		yield return null;
		WaitForSeconds waitForSeconds = new WaitForSeconds(1);

		AsyncOperation ao = SceneManager.LoadSceneAsync(scene);
		ao.allowSceneActivation = false;

		while (!ao.isDone)
		{
			float progress = Mathf.Clamp01(ao.progress / 0.9f);

			yield return waitForSeconds;

			if (LoadDone)
				ao.allowSceneActivation = true;

			yield return null;
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		StartCoroutine(AsynchronousLoad("Sign"));
		StartCoroutine(AsynchronousLoad("LevelMap"));
	}
}
