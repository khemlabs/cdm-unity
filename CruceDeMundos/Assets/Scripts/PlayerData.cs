﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class PlayerData : MonoBehaviour
{
	public int level;
	public int resources;
	public int toolsNumber;
	public int friendsNumber;
	public string date;
	public int imposibleAttempts;
	public List<Level> summary;
	public bool passImposible = false;
	public bool weCanPass = false;

	public enum ToolName
	{
		Matafuegos,
		Restaurador,
		Armonizador
	}

	public GameObject[] tools;

	[JsonObject(MemberSerialization.OptIn)]
	[Serializable]
	public class Level
	{
		[JsonConverter(typeof(StringEnumConverter))]
		public enum LevelState
		{
			LOCKED,
			UNLOCKED,
			PLAYED,
			DONE
		}
		[JsonProperty]
		public int levelNumber;
		[JsonProperty]
		public int playedTimes;
		[JsonProperty]
		public string levelState;
		[JsonProperty]
		public List<bool> objectivesDone = new List<bool>();
	}

	public void UnlockNext()
	{
		Level level = new Level
		{
			levelNumber = Game.Instance.levelManager.leveldata.levelNumber + 1,
			levelState = "UNLOCKED"
		};
		summary.Add(level);
	}

	public void SetSummary()
	{
		int lNumber = Game.Instance.levelManager.leveldata.levelNumber;
		List<bool> oDone = Game.Instance.levelManager.objectivesDone;
		Level level = summary.Find(x => x.levelNumber == lNumber);

		if (level == null)
		{
			level = new Level
			{
				levelNumber = lNumber
			};
			level.playedTimes++;
			level.objectivesDone = oDone;
			summary.Add(level);
		}
		else
		{
			level.playedTimes++;
			level.objectivesDone = oDone;
		}
		int objDone = 0;//Array.FindAll(level.objectivesDone, p => p == true).Length;
		foreach (bool objectiveDone in level.objectivesDone)
		{
			if (objectiveDone) objDone++;
		}
		if (objDone == Game.Instance.levelManager.leveldata.objectives.Count)
			level.levelState = "DONE";
		else
			level.levelState = "PLAYED";

		if (oDone[0])
		{
			UnlockNext();
			if (lNumber == 2)
				friendsNumber = 3;
		}
	}

	public int GetLevelPlayedTimes()
	{
		Level l = summary.Find(x => x.levelNumber == level);
		if (l != null)
			return l.playedTimes;
		else
			return 0;
	}

	public PlayerInfo GetPlayerData()
	{
		PlayerInfo playerInfo = new PlayerInfo(level, resources, toolsNumber, DateTime.Now.ToString(), friendsNumber, summary);

		return playerInfo;
	}

	public void SetPlayerData(PlayerInfo playerInfo)
	{
		level = playerInfo.level;
		resources = playerInfo.resources;
		toolsNumber = playerInfo.toolsNumber;
		date = playerInfo.sessionDate;
		friendsNumber = playerInfo.friendsNumber;
		for (int i = 0; i < playerInfo.summaries.Count; i++)
		{
			Level l = new Level
			{
				levelNumber = playerInfo.summaries[i].levelNumber,
				playedTimes = playerInfo.summaries[i].playedTimes,
				levelState = playerInfo.summaries[i].levelState
			};
			l.objectivesDone = playerInfo.summaries[i].objectivesDone;

			summary.Add(l);
		}
	}
}
