﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootObstacle : MonoBehaviour {

	public enum ObstacleType
	{
		FIRE,
		PORTAL,
		POLLUTION
	}

	public List<GameObject> obstaclesInRange;
	public ObstacleType oType;
	private float lastShotTime;
	private ToolData toolData;
    //public Transform playerTransform;
    public float minimalDistance;
    bool portalgunshooted;
    bool pollutiongunshooted;
    bool extinguishershooted; 
	private GameObject tool;
    public GameObject radioDisplay;

	public bool shooting;
	HealthBar healthBar;

	AudioSource source;

	// Use this for initialization
	void Start() {
		obstaclesInRange = new List<GameObject>();
		lastShotTime = Time.time;
		toolData = gameObject.GetComponentInChildren<ToolData> ();
		tool = gameObject.GetComponent<ToolData> ().CurrentLevel.visualization;

		healthBar = transform.parent.Find("HealthBar").gameObject.GetComponent<HealthBar>();

		source = GetComponent<AudioSource> ();
        portalgunshooted = false;
	}

    /// <summary>
    ///     Rutina para ocultar el radio de disparo luego de 2 segundos
    /// </summary>
    /// <returns></returns>
    IEnumerator Stopper()
    {
        yield return new WaitForSeconds(2);
        //Oculto el radio de disparo del arma
        if (!portalgunshooted || !extinguishershooted || !pollutiongunshooted)
        {
            radioDisplay.SetActive(false);
        }
    }

	// Update is called once per frame
	void Update () {
		GameObject target = null;
		// 1
		float minimalObstacleDistance = float.MaxValue;
		foreach (GameObject obstacle in obstaclesInRange) {
			float distance = Vector3.Distance (obstacle.transform.position, gameObject.transform.position);
			if (distance < minimalObstacleDistance) {
				target = obstacle;
				minimalObstacleDistance = distance;
			}
		}
		// 2
		if (target != null && (Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.Space))) {
            if (target.gameObject.GetComponent<ObstacleData> ().CurrentLevel.health > 0) {
				if (Time.time - lastShotTime > toolData.CurrentLevel.fireRate) {
					if (healthBar.currentHealth > 0)
						shooting = true;
					else {
						shooting = false;
						if (source.isPlaying)
							source.Stop ();
					}
					Shoot (target.GetComponent<Collider2D> ());
					lastShotTime = Time.time;
                    //Se elimina portal
                    if (toolData.toolType == PlayerData.ToolName.Restaurador && Input.GetKeyDown(KeyCode.Space))
                    {
                        portalgunshooted = true;
                    }
                    //Se elimina incendio
                    if (toolData.toolType == PlayerData.ToolName.Matafuegos && Input.GetKey(KeyCode.Space))
                    {
                        extinguishershooted = true;
                    }
                    //Se elimina contaminacion
                    if (toolData.toolType == PlayerData.ToolName.Armonizador && Input.GetKeyDown(KeyCode.Space))
                    {
                        pollutiongunshooted = true;
                    }
                    //Muestro el radio de disparo del arma
                    if (portalgunshooted || extinguishershooted || pollutiongunshooted)
                    {
                        radioDisplay.SetActive(true);
                    }

                    StartCoroutine(Stopper());

                }
                
            } else {
				shooting = false;
				if (source.isPlaying)
					source.Stop ();
                               
            }
            //Frena el disparo de antiportal
            if (portalgunshooted && !extinguishershooted && !pollutiongunshooted && target.gameObject.GetComponent<ObstacleData>().CurrentLevel.health <= 1)
            {
                portalgunshooted = false;
            }
            //Frena disparo de extintor
            if (!portalgunshooted && extinguishershooted && !pollutiongunshooted && target.gameObject.GetComponent<ObstacleData>().CurrentLevel.health < 1 && Input.GetKeyUp(KeyCode.Space))
            {
                extinguishershooted = false;
            }
            //Frena disparo de anticontaminante
            if (!portalgunshooted && !extinguishershooted && pollutiongunshooted && target.gameObject.GetComponent<ObstacleData>().CurrentLevel.health <= 1)
            {
                pollutiongunshooted = false;
            }
            // 3
            Vector3 direction = tool.transform.position - target.transform.position;
			tool.transform.rotation = Quaternion.AngleAxis (90f + Mathf.Atan2 (direction.y, direction.x) * 180 / Mathf.PI, new Vector3 (0, 0, 1));
		}
        else
        {
			tool.transform.localRotation = Quaternion.Euler (Vector3.zero);
			shooting = false;
			if (source.isPlaying)
				source.Stop ();
		}
	}

	// 1
	void OnObstacleDestroy (GameObject obstacle) {
		obstaclesInRange.Remove (obstacle);
	}

	void OnTriggerEnter2D (Collider2D other) {
		// 2
		if (other.gameObject.tag.Equals(oType.ToString())) {
			obstaclesInRange.Add(other.gameObject);
			ObstacleDestructionDelegate del = other.gameObject.GetComponent<ObstacleDestructionDelegate>();
			del.obstacleDelegate += OnObstacleDestroy;
		}
	}
	// 3
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag.Equals(oType.ToString())) {
			obstaclesInRange.Remove(other.gameObject);
			ObstacleDestructionDelegate del =
				other.gameObject.GetComponent<ObstacleDestructionDelegate>();
			del.obstacleDelegate -= OnObstacleDestroy;
		}
	}

	void Shoot(Collider2D target) {
		if (healthBar.currentHealth >= toolData.CurrentLevel.fireLoss) {
			source.Play ();
			shooting = true;
			GameObject bulletPrefab = toolData.CurrentLevel.bullet;
			// 1 
			Vector3 startPosition = gameObject.transform.position;
			Vector3 targetPosition = target.transform.position;
			startPosition.z = bulletPrefab.transform.position.z;
			targetPosition.z = bulletPrefab.transform.position.z;

			// 2 
			GameObject newBullet = (GameObject)Instantiate (bulletPrefab);
			newBullet.transform.position = startPosition;
			BulletBehavior bulletComp = newBullet.GetComponent<BulletBehavior> ();
			bulletComp.target = target.gameObject;
			bulletComp.startPosition = startPosition;
			bulletComp.targetPosition = targetPosition;
						
			healthBar.SetHealth(Mathf.Max (healthBar.currentHealth - toolData.CurrentLevel.fireLoss, 0),toolData.CurrentLevel.fireRate);

			if (healthBar.currentHealth == 0)
				Game.Instance.toolsManager.SetFriendEmpty (transform.parent.gameObject,false);
		}
	}

	public void UpdateTool(){
		tool = gameObject.GetComponent<ToolData> ().CurrentLevel.visualization;
	}
}
