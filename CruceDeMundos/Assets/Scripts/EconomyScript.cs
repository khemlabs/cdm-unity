﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EconomyScript : MonoBehaviour {

    public Text resources;
    public Image image;

    // Use this for initialization
    void Start () {
        //En caso que el GameManager no sea null.
        if (Game.Instance.gameManager != null)
        {
            //Inicializo la cantidad de recursos que se muestra en pantalla.
            resources.text = Game.Instance.gameManager.resources.ToString();
        }
        else
        {
            //Caso contrario lo pongo en "cero"
            //tras el primer update va a tener el verdadero valor.
            resources.text = "0";
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Actualizo la cantidad de contaminantes que se muestra en pantalla.
        resources.text = Game.Instance.gameManager.resources.ToString();
    }
}
