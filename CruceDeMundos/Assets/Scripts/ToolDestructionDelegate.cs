﻿using UnityEngine;
using System.Collections;

public class ToolDestructionDelegate : MonoBehaviour
{

	public delegate void ToolDelegate(GameObject tool);
	public ToolDelegate toolDelegate;

	void OnDestroy()
	{
		if (toolDelegate != null)
		{
			toolDelegate(gameObject);
		}
	}
}
