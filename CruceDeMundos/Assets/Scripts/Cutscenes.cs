﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscenes : MonoBehaviour
{

	public CSDialogManager csdialogManager;
	public GameObject cs_UI;

	// Use this for initialization
	void Start()
	{
		csdialogManager = GetComponent<CSDialogManager>();
		if (Data.Instance.playerData.level == 1)
		{
			//Carga la escena del video de presentación
			Data.Instance.LoadLevel("Game", 1f, 3f, Color.black);
		}
		if (Data.Instance.playerData.level == 7 || Data.Instance.playerData.level == 8)
		{
			//Carga la escena del video de presentación
			Data.Instance.LoadLevel("VideoFinal", 1f, 3f, Color.black);
		}
		else
		{
			//Carga los cutscenes
			Invoke("LoadInitialDialog", 1f);
		}
	}

	void LoadInitialDialog()
	{
		cs_UI.SetActive(true);
		csdialogManager.LoadInitialDialog();
	}

}
