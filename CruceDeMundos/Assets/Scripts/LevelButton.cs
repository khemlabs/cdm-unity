﻿using UnityEngine;
using System.Collections;

public class LevelButton : MonoBehaviour
{

	public int levelNumber;
	public GameObject locked;
	public GameObject unlocked;
	public GameObject played;
	public GameObject done;
	public GameObject stars2;
	public GameObject stars3;
	public string levelState;

	// Use this for initialization
	void Start()
	{
		PlayerData.Level level = Data.Instance.playerData.summary != null ? Data.Instance.playerData.summary.Find(x => x.levelNumber == levelNumber) : null;
		if (level == null)
		{
			if (levelNumber == 1)
			{
				unlocked.SetActive(true);
				levelState = "UNLOCKED";
			}
			else if (Data.Instance.unlockAllLevels)
			{
				unlocked.SetActive(true);
				levelState = "UNLOCKED";
			}
			else
			{
				locked.SetActive(true);
				levelState = "LOCKED";
			}
		}
		else
		{
			if (level.levelState == "LOCKED")
			{
				locked.SetActive(true);
				levelState = "LOCKED";
			}
			else if (level.levelState == "UNLOCKED")
			{
				unlocked.SetActive(true);
				levelState = "UNLOCKED";
			}
			else if (level.levelState == "PLAYED")
			{
				played.SetActive(true);
				levelState = "PLAYED";
			}
			else if (level.levelState == "DONE")
			{
				played.SetActive(true);
				levelState = "PLAYED";
			}
		}
		LevelData.Level ldata = Data.Instance.levelData.levels.Find(x => x.levelNumber == levelNumber);
		if (ldata == null)
		{
			stars3.SetActive(true);
		}
		else
		{
			GameObject stars;
			if (ldata.objectives.Count < 3)
				stars = stars2;
			else
				stars = stars3;

			stars.SetActive(true);
			if (levelState == "PLAYED" || levelState == "DONE")
			{
				Stars s = stars.GetComponent<Stars>();
				for (int i = 0; i < ldata.objectives.Count; i++)
				{
					if (level.objectivesDone[i])
					{
						s.SetStarOn(i);
					}
				}
			}
		}
	}

	public void PlayLevel()
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		Data.Instance.playerData.level = levelNumber;
		Data.Instance.LoadLevel("Game");
	}

	public void PlayCutscene()
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		Data.Instance.csdialogData.ResetAllAtLevel(levelNumber);
		Data.Instance.playerData.level = levelNumber;
		if (levelNumber == 1)
		{
			Data.Instance.LoadLevel("Game", 1f, 3f, Color.black);
		}
		else
		{
			Data.Instance.LoadLevel("Cutscene", 1f, 3f, Color.black);
		}

	}

	public void OnPointerEnter()
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.over);
		Events.OnLevelButtonEnter(levelNumber);
	}

	public void OnPointerExit()
	{
		Events.OnLevelButtonExit();
	}
}
