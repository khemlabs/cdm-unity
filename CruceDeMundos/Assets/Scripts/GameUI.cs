﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System.IO;
using System;
using SimpleJSON;
using UnityEngine.Networking;

public class GameUI : MonoBehaviour
{

	public ResourcesProgress resourcesProgress;

	public GameObject ingameUI;
	public GameObject levelBanner;
	public Text levelNro;
	public Text levelTitle;
	public GameObject mision;
	public GameObject tools;
	public GameObject dialog;
	public GameObject startLevelBanner;
	public GameObject winLevelBanner;
	public GameObject timeOutBanner;
	public GameObject toolsLoseBanner;
	public GameObject siguienteBanner;
	public GameObject siguienteBannerTuto;
	public GameObject iniciarBanner;
	public GameObject tutorial;
	public GameObject summary;
	public TimeProgress timeprogress;
	public GameObject congratsTuto;
	public GameObject fonoReminder;

	public Text[] objectives;

	public GameObject[] objectives_summary;
	public Button nextLevel;
	public Button replay;

	public GameObject stars2;
	public GameObject stars3;
	public Text summaryTitle;

	public ToggleButton fono;
	public ToggleButton btnObjetivos;
	public ToggleButton btnHelp;
	public ToggleButton btnMap;
	public ToggleButton btnSound;

	public GameObject confirm;
	public GameObject help;

	public SelfieUI selfieUI;
	public RawImage selfieImage;
	public Camera selfieCam;
	public RenderTexture selfieRT;

	public int playTimes2GiveUp;
	public int playTimes2Continue;
	public int GiveUpHints;
	public float firstMapTimeInit;
	public Image alarm;
	public Text ingame_levelN;
	public bool firstTutoShowed;
	public bool dataSent;
	string basePath = "./Data/";
	string pathUser = "UserData.json";

	// Use this for initialization
	void Start()
	{
		levelNro.text = "Nivel " + Data.Instance.playerData.level;
		levelTitle.text = Game.Instance.levelManager.leveldata.title;

		ingame_levelN.text = "Nivel " + Data.Instance.playerData.level;

		resourcesProgress.OnRefreshResources(Data.Instance.playerData.resources);

		for (int i = 0; i < objectives.Length; i++)
		{
			if (i < Game.Instance.levelManager.leveldata.objectives.Count)
				objectives[i].text = Game.Instance.levelManager.leveldata.objectives[i];
			if (objectives[i].text != "")
			{
				objectives_summary[i].SetActive(true);
				objectives_summary[i].transform.Find("Text").gameObject.GetComponent<Text>().text = objectives[i].text;
				if (i == 2)
					stars3.SetActive(true);
			}
			else if (i == 2)
			{
				stars2.SetActive(true);
			}
		}

		firstTutoShowed = false;

		ingameUI.SetActive(false);
		levelBanner.SetActive(false);

		Events.GameIntro += GameIntro;
		Events.GameMision += GameMision;
		Events.GameTools += GameTools;
		Events.GameAutoeval += GameAutoeval;
		Events.GameReady += GameReady;
		Events.GameActive += GameActive;
		Events.GameMap += GameMap;
		Events.GameDialog += GameDialog;

		Events.StartGame += StartGame;

		Events.OnObjectiveDone += OnObjectiveDone;
		Events.OnTimeOut += OnTimeOut;
		Events.OnToolsLose += OnToolsLose;

		Events.OnLevelEndDialog += OnLevelEndDialog;

		btnSound.defaultValue = !Data.Instance.mute;

		if (Data.Instance.avatarData.selfie != null)
		{
			selfieImage.texture = Data.Instance.avatarData.selfie;
		}
		dataSent = false;
	}

	void OnDestroy()
	{
		Events.GameIntro -= GameIntro;
		Events.GameMision -= GameMision;
		Events.GameTools -= GameTools;
		Events.GameAutoeval -= GameAutoeval;
		Events.GameReady -= GameReady;
		Events.GameActive -= GameActive;
		Events.GameMap -= GameMap;
		Events.GameDialog -= GameDialog;

		Events.StartGame -= StartGame;

		Events.OnObjectiveDone -= OnObjectiveDone;
		Events.OnTimeOut -= OnTimeOut;
		Events.OnToolsLose -= OnToolsLose;

		Events.OnLevelEndDialog -= OnLevelEndDialog;
	}

	void GameIntro()
	{
		levelBanner.SetActive(true);
		Invoke("HideLevel", 2);
	}

	void HideLevel()
	{
		if (Game.Instance.levelManager.leveldata.isTuto && !firstTutoShowed)
		{
			siguienteBannerTuto.SetActive(true);
			firstTutoShowed = true;
		}
		else
		{
			siguienteBanner.SetActive(true);
		}

		firstMapTimeInit = Time.realtimeSinceStartup;
		levelBanner.SetActive(false);
	}

	void GameMision()
	{
		if (siguienteBannerTuto.activeInHierarchy) siguienteBannerTuto.SetActive(false);
		if (siguienteBanner.activeInHierarchy) siguienteBanner.SetActive(false);
		Game.Instance.levelMetrics.firstMapTime = Time.realtimeSinceStartup - firstMapTimeInit;
		mision.SetActive(true);
	}

	public void ShowMision()
	{
		mision.SetActive(true);
		btnObjetivos.SetButtonOn(true);
	}

	public void CloseMision()
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		if (Game.Instance.gameManager.state == GameManager.States.MISION)
		{
			Game.Instance.gameManager.AutoEval();
		}
		else
		{
			mision.SetActive(false);
			btnObjetivos.SetButtonOn(false);
		}
	}

	void GameTools()
	{
		tools.SetActive(true);
		Game.Instance.levelMetrics.toolsBeginTime = Time.realtimeSinceStartup;
		dialog.SetActive(false);
	}

	void GameAutoeval()
	{
		mision.SetActive(false);
		dialog.SetActive(true);
	}

	/// <summary>
	/// Esto arranca el nivel luego de los dialogos iniciales, escenas y objetivos mostrados.
	/// </summary>
	void GameActive()
	{
		fono.SetButtonOn(false);
		dialog.SetActive(false);
		ingameUI.SetActive(true);
		if (Game.Instance.levelManager.leveldata.isTuto)
		{
			tutorial.SetActive(true);
		}
		else
		{
			tutorial.SetActive(false);
		}
	}

	void GameMap()
	{
		Game.Instance.levelMetrics.mapCheck++;
		fono.SetButtonOn(true);
	}

	void GameDialog()
	{
		ingameUI.SetActive(false);
		dialog.SetActive(true);
	}

	void GameReady()
	{
		tools.SetActive(false);
		Game.Instance.levelMetrics.toolsEndTime = Time.realtimeSinceStartup;
		dialog.SetActive(false);
		iniciarBanner.SetActive(true);
	}

	void StartGame()
	{
		iniciarBanner.SetActive(false);
		startLevelBanner.SetActive(true);

		Invoke("HideStartGame", 1);
	}

	void HideStartGame()
	{
		startLevelBanner.SetActive(false);
		Events.GameActive();
		Game.Instance.gameManager.state = GameManager.States.ACTIVE;
		Game.Instance.levelMetrics.levelBeginTime = Time.realtimeSinceStartup;
	}

	void OnObjectiveDone()
	{
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		Game.Instance.levelManager.CheckTimeObjective(Game.Instance.levelMetrics.levelTime);
		Game.Instance.levelManager.CheckChargeObjective();
		ingameUI.SetActive(false);
		summary.SetActive(true);
		if (summary.activeInHierarchy && Game.Instance.levelManager.leveldata.isTuto)
		{
			congratsTuto.SetActive(true);
		}
		else
		{
			congratsTuto.SetActive(false);
		}

		summaryTitle.text = "¡MISIÓN CUMPLIDA!";
		Game.Instance.levelManager.objectivesDone[0] = true;
		Data.Instance.playerData.resources += Game.Instance.levelManager.leveldata.resourceWin;
		Events.OnRefreshResources(Data.Instance.playerData.resources);
		Game.Instance.gameManager.state = GameManager.States.WIN;

		if (Game.Instance.dialogManager.LoadFinalDialog())
		{
			summary.SetActive(false);
			Events.GameDialog();
		}
		else
		{
			Game.Instance.ingameMusic.MusicWin();
		}

		SetSummary();
		dataSent = true;
	}

	void OnLevelEndDialog()
	{
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		summary.SetActive(true);
		Game.Instance.ingameMusic.MusicWin();
	}

	void OnTimeOut()
	{
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		ingameUI.SetActive(false);
		summary.SetActive(true);
		nextLevel.interactable = false;
		summaryTitle.text = "SE ACABÓ EL TIEMPO, INTENTALO NUEVAMENTE";
		Game.Instance.levelManager.objectivesDone[0] = false;
		Game.Instance.gameManager.state = GameManager.States.LOSE;
		Game.Instance.ingameMusic.MusicTimeOut();
		if (Game.Instance.levelManager.leveldata.isImposible)
		{
			if (Data.Instance.playerData.GetLevelPlayedTimes() < GiveUpHints)
			{
				summaryTitle.text = "SE ACABÓ EL TIEMPO";
			}
			else
			{
				summaryTitle.fontSize = 30;
				summaryTitle.text = "SE ACABÓ EL TIEMPO, PODÉS PASAR AL PRÓXIMO NIVEL CUANDO QUIERAS";
			}
		}
		if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 7;
		if (Game.Instance.levelManager.leveldata.isImposible && Game.Instance.levelMetrics.endLevel == 7)
		{
			Game.Instance.levelMetrics.endLevel = 4;
		}

		SetSummary();
		dataSent = true;

	}

	void OnToolsLose()
	{
		Game.Instance.levelMetrics.levelTime = Game.Instance.levelManager.leveldata.timeOut - Game.Instance.gameUI.timeprogress.time;
		ingameUI.SetActive(false);
		summary.SetActive(true);
		nextLevel.interactable = false;
		summaryTitle.text = "TE QUEDASTE SIN HERRAMIENTAS, INTENTALO NUEVAMENTE";
		Game.Instance.levelManager.objectivesDone[0] = false;
		Game.Instance.gameManager.state = GameManager.States.LOSE;
		Game.Instance.ingameMusic.MusicWin();
		if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 8;
		SetSummary();
		dataSent = true;
	}

	// Update is called once per frame
	void Update()
	{
		if (fonoReminder.activeInHierarchy && Input.GetKeyDown(KeyCode.Z) && Game.Instance.levelManager.leveldata.isTuto)
		{
			fonoReminder.SetActive(false);
		}

		if (!Game.Instance.levelManager.leveldata.isTuto) fonoReminder.SetActive(false);

	}

	void SetSummary()
	{
		//Logica del nivel imposible 
		if (Game.Instance.levelManager.leveldata.isImposible)
		{
			if (Data.Instance.playerData.GetLevelPlayedTimes() > 0)
			{
				nextLevel.interactable = true;
			}
		}
		AlarmEnable(false);
		Game.Instance.gameManager.state = GameManager.States.ENDED;
		Game.Instance.levelMetrics.levelEndTime = Time.realtimeSinceStartup;
		Stars s2 = stars2.GetComponent<Stars>();
		Stars s3 = stars3.GetComponent<Stars>();
		StringBuilder builder = new StringBuilder();
		int i = 0;
		foreach (bool objectiveDone in Game.Instance.levelManager.objectivesDone)
		{
			objectives_summary[i].transform.Find("done").gameObject.SetActive(objectiveDone);
			objectives_summary[i].transform.Find("undone").gameObject.SetActive(!objectiveDone);
			if (i < 2)
				if (objectiveDone)
					s2.SetStarOn(i);
			if (objectiveDone)
				s3.SetStarOn(i);
			builder.Append(objectiveDone + ";");
			i++;
		}
		string misions;
		misions = builder.ToString();
		if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 0;
		Data.Instance.playerData.resources = Game.Instance.gameManager.resources;
		Events.OnRefreshResources(Data.Instance.playerData.resources);
		Game.Instance.levelMetrics.SaveLevelData(misions);
		Data.Instance.playerData.SetSummary();
		if (Data.Instance.SyncServerMode)
		{
			saveGameAsync();
		}
		else
		{
			Data.Instance.SaveGameData();
		}
	}

	public void Final()
	{
		Data.Instance.playerData.resources += Game.Instance.levelManager.leveldata.resourceWin;
		Events.OnRefreshResources(Data.Instance.playerData.resources);
		Game.Instance.gameManager.state = GameManager.States.ENDED;
		Game.Instance.levelMetrics.levelEndTime = Time.realtimeSinceStartup;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < Game.Instance.levelManager.objectivesDone.Count; i++)
		{
			Game.Instance.levelManager.objectivesDone[i] = true;
			if (i < Game.Instance.levelManager.leveldata.objectives.Count)
				builder.Append(Game.Instance.levelManager.objectivesDone[i] + ";");
		}
		string misions;
		misions = builder.ToString();
		if ((Game.Instance.levelMetrics.endLevel == -1)) Game.Instance.levelMetrics.endLevel = 0;
		Game.Instance.levelMetrics.SaveLevelData(misions);
		Data.Instance.playerData.SetSummary();
		if (Data.Instance.SyncServerMode)
		{
			saveGameAsync();
		}
		else
		{
			Data.Instance.SaveGameData();
		}
	}

	async void saveGameAsync()
	{
		await Data.Instance.SaveWebGameData();
	}

	public void SoundToggle(bool on)
	{
		btnSound.SetButtonOn(on);
		Data.Instance.Mute(!on);
	}

	public void HelpToggle(bool on)
	{
		btnHelp.SetButtonOn(on);
		help.SetActive(on);
	}

	public void LevelMap(bool on)
	{
		btnMap.SetButtonOn(on);
		confirm.SetActive(on);
	}

	public void SelfieEdit(bool on)
	{
		selfieCam.enabled = on;
		selfieUI.gameObject.SetActive(on);
		if (on)
			Game.Instance.gameManager.state = GameManager.States.SELFIE;
		else
			Game.Instance.gameManager.state = GameManager.States.MAP;
	}

	public void SelfieDone()
	{
		selfieCam.enabled = false;
		Data.Instance.avatarData.CaptureSelfie(selfieRT);
		selfieImage.texture = Data.Instance.avatarData.selfie;

		Data.Instance.avatarData.SaveAvatarData(Data.Instance.userId.ToUpper());
		Data.Instance.SaveSelfie(selfieUI.emoji, selfieUI.estado.text);
		selfieUI.gameObject.SetActive(false);
		Game.Instance.gameManager.state = GameManager.States.MAP;
	}

	public void AlarmEnable(bool enable)
	{
		alarm.gameObject.SetActive(enable);
	}
}