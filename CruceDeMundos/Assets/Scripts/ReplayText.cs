﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplayText : MonoBehaviour
{
    public Text replay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Game.Instance.levelManager.leveldata.isImposible)
        {
            if(Data.Instance.playerData.GetLevelPlayedTimes() < 5)
            {
                replay.text = "Mas tiempo";
            }
            else
            {
                replay.text = "Rejugar";
            }
        }
        else
        {
            replay.text = "Rejugar";
        }
        
    }
}
