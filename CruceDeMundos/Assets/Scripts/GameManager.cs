﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

	public float time;
	public int tools;
	public int portals;
	public int fires;
	public int pollutions;
	public int resources;
	public int totalInvested;
	public int imposibleAttempts;
	public float mapTimeInit;
	public float mapTimeEnd;
	public DialogAnimation dialogAnimator;

	public States state;
	public enum States
	{
		INTRO,
		MISION,
		TOOLS,
		AUTOEVAL,
		ACTIVE,
		MAP,
		SELFIE,
		DIALOG,
		WIN,
		LOSE,
		ENDED
	}

	bool gameStarted = false;

	// Use this for initialization
	void Start()
	{
		state = States.INTRO;
		Events.DialogDone += DialogDone;

		if (Data.Instance.playerData.level != 1)
		{
			resources = Data.Instance.playerData.resources;
		}
		else
		{
			if (Data.Instance.playerData.resources <= 100)
			{
				resources = 100;
				Data.Instance.playerData.resources = resources;
			}
			else
			{
				resources = Data.Instance.playerData.resources;
			}

		}
		Game.Instance.levelMetrics.levelBeginTime = Time.realtimeSinceStartup;
		Game.Instance.levelMetrics.rtBegin = Data.Instance.playerData.resources;
		totalInvested = 0;
		imposibleAttempts = 0;

		Invoke("Intro", 1);
	}

	void OnDestroy()
	{
		Events.DialogDone -= DialogDone;
	}

	void Intro()
	{
		Events.GameIntro();
	}

	public void Mision()
	{
		Game.Instance.levelMetrics.objectivesBeginTime = Time.realtimeSinceStartup;
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		state = States.MISION;
		Events.GameMision();
	}

	public void Tools()
	{
		Game.Instance.levelMetrics.toolsBeginTime = Time.realtimeSinceStartup;
		state = States.TOOLS;
		Events.GameTools();
	}

	public void AutoEval()
	{
		Game.Instance.levelMetrics.objectivesEndTime = Time.realtimeSinceStartup;

		if (Game.Instance.dialogManager.LoadInitialDialog())
		{
			Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
			state = States.AUTOEVAL;
			Events.GameAutoeval();
		}
		else
		{
			Tools();
		}
	}

	void DialogDone()
	{
		if (!gameStarted)
		{
			Tools();
		}
		else
		{
			state = States.ACTIVE;
			Events.GameActive();
		}

		Invoke("ResetCharacterCollider", 2f);
	}

	public void GameReady()
	{
		Game.Instance.toolsManager.SelectedToolsData();
		Game.Instance.levelMetrics.toolsEndTime = Time.realtimeSinceStartup;
		Game.Instance.levelMetrics.map2BeginTime = Time.realtimeSinceStartup;
		mapTimeInit = Time.realtimeSinceStartup;
		Game.Instance.levelMetrics.rtPostTools = Data.Instance.playerData.resources;
		portals = Game.Instance.levelManager.leveldata.portalNumber;
		fires = Game.Instance.levelManager.leveldata.fireNumber;
		pollutions = Game.Instance.levelManager.leveldata.pollutionNumber;
		if (Data.Instance.playerData.level != 1)
		{
			resources = Data.Instance.playerData.resources;
		}
		else
		{
			if (!(Data.Instance.playerData.resources > 100))
			{
				resources = 100;
				Data.Instance.playerData.resources = resources;
			}
		}
		imposibleAttempts = Data.Instance.playerData.imposibleAttempts;
		if (Game.Instance.levelManager.leveldata.isImposible)
		{
			imposibleAttempts += 1;
			Data.Instance.playerData.imposibleAttempts = imposibleAttempts;
			Data.Instance.playerData.weCanPass = false;
			Data.Instance.playerData.passImposible = false;
		}

		Events.GameReady();
	}

	void ResetCharacterCollider()
	{
		Events.ResetCharacterCollider();
	}

	public void StartGame()
	{
		Game.Instance.levelMetrics.map1EndTime += Time.realtimeSinceStartup - Game.Instance.levelMetrics.map2BeginTime;
		//Tools Map_Time
		Game.Instance.levelMetrics.toolsMapTime = Time.realtimeSinceStartup - mapTimeInit;
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.click1);
		Events.StartGame();
		gameStarted = true;
	}

	public void Phone()
	{
		if (state == States.MAP)
		{
			//2do y n-enesimos Map_Time
			Game.Instance.levelMetrics.phoneMapTime += Time.realtimeSinceStartup - mapTimeInit;
			state = States.ACTIVE;
			if (Game.Instance.levelManager.leveldata.isTuto) Game.Instance.gameUI.fonoReminder.SetActive(true);
			Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.phoneClose);
			Game.Instance.gameUI.tutorial.SetActive(true);
			Events.GameActive();
		}
		else
		{
			mapTimeInit = Time.realtimeSinceStartup;
			state = States.MAP;
			if (Game.Instance.levelManager.leveldata.isTuto) Game.Instance.gameUI.fonoReminder.SetActive(false);
			Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.phoneOpen);
			Game.Instance.gameUI.tutorial.SetActive(false);
			Events.GameMap();
		}
	}

	// Update is called once per frame
	void Update()
	{
		QualitySettings.vSyncCount = 1;  // VSync must be disabled
																		 //Application.targetFrameRate = 45;
	}
}
