using UnityEngine;
using System.Runtime.InteropServices;
public static class EnvironmentScript
{
#if UNITY_WEBGL && !UNITY_EDITOR
	[DllImport("__Internal")] public static extern string GetVariablesString();
#else
	public static string GetVariablesString()
	{
		//Void function to avoid compile errors
		return "";
	}
#endif

	public static string GetEnvironmentJsonString()
	{
		Debug.Log("Environment String " + GetVariablesString());
		return GetVariablesString();
	}
}