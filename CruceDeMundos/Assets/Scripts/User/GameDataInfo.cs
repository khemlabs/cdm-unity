﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class GameDataInfo
{
	[JsonProperty]
	public PlayerInfo playerInfo;
	[JsonProperty]
	public DialogData.DialogResponseInfo dialogResponseInfo;

	public GameDataInfo(PlayerInfo playerInfo, DialogData.DialogResponseInfo dialogResponseInfo)
	{
		this.playerInfo = playerInfo;
		this.dialogResponseInfo = dialogResponseInfo;
	}
}