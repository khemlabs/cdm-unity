﻿using System.Collections.Generic;

public class SummaryInfo
{
	int levelNumber;
	int playedTimes;
	string levelState;
	List<bool> objectivesDone;

	public SummaryInfo(int levelNumber, int playedTimes, string levelState, List<bool> objectivesDone)
	{
		this.levelNumber = levelNumber;
		this.playedTimes = playedTimes;
		this.levelState = levelState;
		this.objectivesDone = new List<bool>(objectivesDone);
	}
}