﻿using System;
using System.Collections;
using System.Collections.Generic;

public class LevelInfo
{
	/*
	builder.Append("{");
							builder.Append("level:" + dialogCharacters[i].levelsInfo[j].level + ",");
							builder.Append("emoval:" + dialogCharacters[i].levelsInfo[j].emoval + ",");
							builder.Append("goTo:" + dialogCharacters[i].levelsInfo[j].goTo + ",");
							builder.Append("lastExpre:" + dialogCharacters[i].levelsInfo[j].lastExpre + ",");
							builder.Append("dtype:" + dialogCharacters[i].levelsInfo[j].dtype);
							builder.Append("}");
	 */

	public int level;
	public int emoval;
	public int goTo;
	public string lastExpre;
	public string dType;
}