﻿using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class Avatar
{
	[JsonProperty]
	public int pielIndex;
	[JsonProperty]
	public int peloIndex;
	[JsonProperty]
	public int peloColorIndex;
	[JsonProperty]
	public int caraIndex;
	[JsonProperty]
	public int torsoIndex;
	[JsonProperty]
	public int piernasIndex;
	[JsonProperty]
	public int zapatoIndex;
	[JsonProperty]
	public int estadoIndex;

	public Avatar(int pielIndex, int peloIndex, int peloColorIndex, int caraIndex, int torsoIndex, int piernasIndex, int zapatoIndex, int estadoIndex)
	{
		this.pielIndex = pielIndex;
		this.peloIndex = peloIndex;
		this.peloColorIndex = peloColorIndex;
		this.caraIndex = caraIndex;
		this.torsoIndex = torsoIndex;
		this.piernasIndex = piernasIndex;
		this.zapatoIndex = zapatoIndex;
		this.estadoIndex = estadoIndex;
	}

	public int getPielIndex()
	{
		return pielIndex;
	}
	public int getPeloIndex()
	{
		return peloIndex;
	}
	public int getPeloColorIndex()
	{
		return peloColorIndex;
	}
	public int getCaraIndex()
	{
		return caraIndex;
	}
	public int getTorsoIndex()
	{
		return torsoIndex;
	}
	public int getPiernasIndex()
	{
		return piernasIndex;
	}
	public int getZapatoIndex()
	{
		return zapatoIndex;
	}
	public int getEstadoIndex()
	{
		return estadoIndex;
	}
}