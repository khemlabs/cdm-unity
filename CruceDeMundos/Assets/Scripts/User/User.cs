﻿

using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class User
{
	[JsonProperty]
	private string userId;
	[JsonProperty]
	private string userName;


	public User(string newUserId, string newUserName)
	{
		userId = newUserId;
		userName = newUserName;
	}

	public void setUserId(string userId)
	{
		this.userId = userId;
	}


	public void setUserName(string userName)
	{
		this.userName = userName;
	}

	public string getUserId()
	{
		return userId;
	}


	public string getUserName()
	{
		return userName;
	}
}
