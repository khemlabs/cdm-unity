﻿using System.Collections.Generic;
using Newtonsoft.Json;
using static PlayerData;

[JsonObject(MemberSerialization.OptIn)]
public class PlayerInfo
{
	[JsonProperty]
	public int level;
	[JsonProperty]
	public int resources;
	[JsonProperty]
	public int toolsNumber;
	[JsonProperty]
	public string sessionDate;
	[JsonProperty]
	public int friendsNumber;
	[JsonProperty]
	public List<Level> summaries;

	public PlayerInfo(int level, int resources, int toolsNumber, string sessionDate, int friendsNumber, List<Level> summaries)
	{
		this.level = level;
		this.resources = resources;
		this.toolsNumber = toolsNumber;
		this.sessionDate = sessionDate;
		this.friendsNumber = friendsNumber;
		this.summaries = new List<Level>(summaries);
	}
}