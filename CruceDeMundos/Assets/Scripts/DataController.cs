﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;
using System.Net;

public class DataController : MonoBehaviour
{
	private string createUser_URL;
	private string checkUser_URL;
	private string addLevel_URL;
	private string addReply_URL;
	private string addDialog_URL;
	private string addMission_URL;
	private string addLevelData_URL;
	private string addSelfie_URL;
	private string getCursos_URL;
	private string getSession_URL;
	private readonly string secretKey = "mondongo";
	public string UserID;
	double gameid;
	float startTime;
	private List<string> sendMessages;
	private List<string> pendingMessages;

	String content;
	// Use this for initialization
	void Start()
	{
		sendMessages = new List<string>();
		pendingMessages = new List<string>();

		DateTime now = DateTime.Now;
		gameid = now.Month * 2592000000 + now.Day * 86400000 + now.Hour * 3600000 + now.Minute * 60000 + now.Second * 1000 + now.Millisecond;

		startTime = Time.realtimeSinceStartup;
		UserID = "";
	}

	// Update is called once per frame
	void Update()
	{
		//Cada 60 segundos intentamos enviar cualquier tipo de información que no se pudo enviar.
		if (Time.realtimeSinceStartup - startTime >= 60 && UserID != "" && pendingMessages.Count > 0)
		{
			RetrySendData();
			startTime = Time.realtimeSinceStartup;
		}
	}

	public IEnumerator CreateUserRoutine(string _userid, string _compid, string _username, string cue, string grado, string division, string turno)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(secretKey);
		if (_userid != "" && _compid != "")
		{
			string post_url;
			createUser_URL = URL + "/users/update?";
			StringBuilder builder = new StringBuilder();

			builder.Append(createUser_URL);
			builder.Append("userid=");
			builder.Append(UnityWebRequest.EscapeURL(_userid.ToUpper()));
			builder.Append("&computerid=");
			builder.Append(UnityWebRequest.EscapeURL(_compid));
			builder.Append("&username=");
			builder.Append(UnityWebRequest.EscapeURL(_username));
			builder.Append("&cue=");
			builder.Append(UnityWebRequest.EscapeURL(cue));
			builder.Append("&grado=");
			builder.Append(UnityWebRequest.EscapeURL(grado));
			builder.Append("&division=");
			builder.Append(UnityWebRequest.EscapeURL(division));
			builder.Append("&turno=");
			builder.Append(UnityWebRequest.EscapeURL(turno));
			builder.Append("&hash=");
			builder.Append(hash);

			post_url = builder.ToString();

			print("CreateUser : " + post_url);
			using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
			{
				// Request and wait for the desired page.
				yield return webRequest.SendWebRequest();

				if (webRequest.isNetworkError || webRequest.isHttpError)
				{
					Debug.Log("No pudo crear el nuevo user: " + webRequest.error);

					//Si hay inconvenientes limpiamos la información local del ultimo usuario
					//Forbidden Access
					if (webRequest.responseCode == 403)
					{
						Data.Instance.Reset();
					}
				}
				else
				{
					//Si hay inconvenientes limpiamos la información local del ultimo usuario
					if (webRequest.downloadHandler.text == "FORBIDDEN ACCESS")
					{
						Data.Instance.Reset();
					}
					//Aquí se guardan los datos del usuario
					User user = new User(_userid.ToUpper(), _username);
					string json = JsonConvert.SerializeObject(user);

					new SaverUtil().saveData(_userid.ToUpper() + "userData", json);

					print("user agregado: " + webRequest.downloadHandler.text);
					Debug.Log("user agregado: " + webRequest.downloadHandler.text);
				}
			}
		}
	}

	/// <summary>
	///  Guardado de los datos del nivel jugado.
	/// </summary>
	/// <param name="_userid">Id de usuario</param>
	/// <param name="_compid">Id de computadora</param>
	/// <param name="level">Nivel</param>
	/// <param name="tools">Herramientas seleccionadas</param>
	/// <param name="toolsEnd">Lo que quedo de las herramientas</param>
	/// <param name="missions">Objetivos completados o no</param>
	/// <param name="portalDone">Cuantos portales se cerraron</param>
	/// <param name="fireDone">Cuantos incendios se apagaron</param>
	/// <param name="pollutionDone">Cuantas contaminaciones se cerraron</param>
	/// <param name="mapchecks">Cuantas veces el jugador vio el mapa</param>
	/// <param name="levelTime">Tiempo invertido para completar el nivel(sin contar las pausas)</param>
	/// <param name="gameTime">Tiempo invertido en juego(cuenta todo)</param>
	/// <param name="firstMapTime">Tiempo que el jugador que ve el mapa por primera vez</param>
	/// <param name="toolsMapTime">Tiempo que el jugador que ve el mapa tras elegir herramientas</param>
	/// <param name="phoneMapTime">Tiempo que el jugador que ve el mapa tras abrir el telefono</param>
	/// <param name="missionTime">Tiempo que el jugador ve los objetivos de la mision</param>
	/// <param name="toolsTime">Tiempo que se demora el jugador en elegir herramientas</param>
	/// <param name="mapTrail">Recorrido del mapa</param>
	/// <param name="mapDeadEnds">Del recorrido cuales fueron callejones sin salida</param>
	/// <param name="resourcesBefore">Recursos Tecnologicos antes de elegir herramientas</param>
	/// <param name="resourcesAfterTools">Recursos Tecnologicos despues de elegir herramientas</param>
	/// <param name="resourcesAtEnd">Recursos Tecnologicos al final del nivel</param>
	/// <param name="portalCharge">Cargas de arma anti portal recolectadas</param>
	/// <param name="fireCharge">Cargas de matafuegos recolectados</param>
	/// <param name="pollutionCharge">Cargas de anti contaminate recolectados</param>
	/// <param name="resourcesCharge">Cargas de recursos tecnologicos recolectados</param>
	/// <param name="endLevel">Salida del nivel ()</param>
	/// <returns></returns>
	public IEnumerator SaveLevelData(string _userid, string _compid, int level, string tools, string toolsEnd, string missions, int portalDone, int fireDone, int pollutionDone, int mapchecks, float levelTime, float gameTime, float firstMapTime, float toolsMapTime, float phoneMapTime, float missionTime, float toolsTime, string mapTrail, string mapDeadEnds, int resourcesBefore, int resourcesAfterTools, int resourcesAtEnd, int portalCharge, int fireCharge, int pollutionCharge, int resourcesCharge, int endLevel)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(_userid.ToUpper() + _compid + secretKey);
		addLevel_URL = URL + "/level/add?";
		string post_url;
		StringBuilder builder = new StringBuilder();

		builder.Append(addLevel_URL);
		builder.Append("user_id=");
		builder.Append(UnityWebRequest.EscapeURL(_userid.ToUpper()));
		builder.Append("&computer_id=");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&game_id=");
		builder.Append(gameid.ToString());
		builder.Append("-");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&level_id=");
		builder.Append(level);
		builder.Append("&tools_selected=");
		builder.Append(UnityWebRequest.EscapeURL(tools));
		builder.Append("&tools_end=");
		builder.Append(UnityWebRequest.EscapeURL(toolsEnd));
		builder.Append("&missions=");
		builder.Append(UnityWebRequest.EscapeURL(missions));
		builder.Append("&portal_done=");
		builder.Append(portalDone);
		builder.Append("&fire_done=");
		builder.Append(fireDone);
		builder.Append("&pollution_done=");
		builder.Append(pollutionDone);
		builder.Append("&map_checks=");
		builder.Append(mapchecks);
		builder.Append("&level_time=");
		builder.Append(levelTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&game_time=");
		builder.Append(gameTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&first_map_time=");
		builder.Append(firstMapTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&tools_map_time=");
		builder.Append(toolsMapTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&phone_map_time=");
		builder.Append(phoneMapTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&mission_time=");
		builder.Append(missionTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&tools_time=");
		builder.Append(toolsTime.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&map_trail=");
		builder.Append(UnityWebRequest.EscapeURL(mapTrail));
		builder.Append("&map_dead_ends=");
		builder.Append(UnityWebRequest.EscapeURL(mapDeadEnds));
		builder.Append("&rt_begin=");
		builder.Append(resourcesBefore);
		builder.Append("&rt_after_tools=");
		builder.Append(resourcesAfterTools);
		builder.Append("&rt_end=");
		builder.Append(resourcesAtEnd);
		builder.Append("&portal_charge=");
		builder.Append(portalCharge);
		builder.Append("&fire_charge=");
		builder.Append(fireCharge);
		builder.Append("&pollution_charge=");
		builder.Append(pollutionCharge);
		builder.Append("&rt_charge=");
		builder.Append(resourcesCharge);
		builder.Append("&end_level=");
		builder.Append(endLevel);
		builder.Append("&hash=");
		builder.Append(hash);

		post_url = builder.ToString();

		if (!levelDataSent(post_url))
		{
			print("addLevel : " + post_url);
			using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
			{
				// Request and wait for the desired page.
				yield return webRequest.SendWebRequest();

				if (webRequest.isNetworkError || webRequest.isHttpError)
				{
					Debug.LogError("No pudo agregar datos de nivel: " + webRequest.error);
					pendingMessages.Add(post_url);
				}
				else
				{
					print("datos de nivel agregados: " + webRequest.downloadHandler.text);
					Debug.Log("datos de nivel agregados: " + webRequest.downloadHandler.text);
					sendMessages.Add(post_url);
				}
			}
		}
	}

	bool levelDataSent(string post_url)
	{
		bool alreadySent = false;
		string[] post_urlSplited = post_url.Substring(post_url.IndexOf('?')).Split('&');

		foreach (string sent in sendMessages)
		{
			string[] sentSplited = sent.Substring(post_url.IndexOf('?')).Split('&');
			if (sentSplited.Length > 27)
			{
				if (
								sentSplited[0] == post_urlSplited[0] && //user_id
								sentSplited[1] == post_urlSplited[1] && //computer_id
								sentSplited[2] == post_urlSplited[2] && //game_id
								sentSplited[3] == post_urlSplited[3] && //level_id
								sentSplited[10] == post_urlSplited[10] && //map_checks
								sentSplited[13] == post_urlSplited[13] && //first_map_time
								sentSplited[14] == post_urlSplited[14] && //tools_map_time
								sentSplited[15] == post_urlSplited[15] && //phone_map_time
								sentSplited[16] == post_urlSplited[16] && //mission_time
								sentSplited[27] == post_urlSplited[27] //level_end
								)
				{
					alreadySent = true;
				}
			}
		}

		return alreadySent;
	}

	/// <summary>
	///  Guardado de los dialogos al servidor, en caso de falla existe un fail safe que guarda en archivo o en resgistro de Windows
	/// </summary>
	/// <param name="_userid">Identificador de usuario</param>
	/// <param name="_compid">Identificador de computadora</param>
	/// <param name="character">Nombre de personaje</param>
	/// <param name="level">Nivel</param>
	/// <param name="index">Indice de dialogo dentro del arbol de dialogo</param>
	/// <param name="mood">Estado de animo</param>
	/// <param name="answerId">indice de respuesta</param>
	/// <param name="time">Momento en que se graba la respuesta</param>
	/// <param name="dialogId">Identificador de dialogo para la base</param>
	/// <returns></returns>
	public IEnumerator SaveDialogData(string _userid, string _compid, string character, int level, int index, string mood, int answerId, float time, int dialogId)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(_userid.ToUpper() + _compid + secretKey);
		addReply_URL = URL + "/reply/add?";
		string post_url;
		StringBuilder builder = new StringBuilder();

		builder.Append(addReply_URL);
		builder.Append("user_id=");
		builder.Append(UnityWebRequest.EscapeURL(_userid.ToUpper()));
		builder.Append("&computer_id=");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&game_id=");
		builder.Append(gameid.ToString());
		builder.Append("-");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&character_name=");
		builder.Append(UnityWebRequest.EscapeURL(character));
		builder.Append("&dialog_id=");
		builder.Append(dialogId);
		builder.Append("&level_id=");
		builder.Append(level);
		builder.Append("&dialog_index=");
		builder.Append(index);
		builder.Append("&dialog_mood=");
		builder.Append(UnityWebRequest.EscapeURL(mood));
		builder.Append("&answer_id=");
		builder.Append(answerId);
		builder.Append("&dialog_time=");
		builder.Append(time.ToString("G", CultureInfo.InvariantCulture));
		builder.Append("&hash=");
		builder.Append(hash);

		post_url = builder.ToString();

		print("addReply : " + post_url);
		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No pudo agregar datos de nivel: " + webRequest.error);
				pendingMessages.Add(post_url);
			}
			else
			{
				print("reply agregada: " + webRequest.downloadHandler.text);
				Debug.Log("reply agregada: " + webRequest.downloadHandler.text);
				sendMessages.Add(post_url);
			}
		}
	}

	public IEnumerator AddDialog(string character, int level, int index, string dType, string mood, string prompt, int answerId,
		string answer, string replyType, string replySubType, string indicVal)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(secretKey);
		addDialog_URL = URL + "/dialog/add?";

		string post_url = addDialog_URL + "&character_name=" + UnityWebRequest.EscapeURL(character) + "&level_id=" + level +
			"&dialog_index=" + index + "&dialog_type=" + UnityWebRequest.EscapeURL(dType) + "&dialog_mood=" + UnityWebRequest.EscapeURL(mood) +
			"&dialog_prompt=" + UnityWebRequest.EscapeURL(prompt) + "&answer_id=" + answerId + "&answer_text=" + UnityWebRequest.EscapeURL(answer) +
			"&tipo_indicador=" + UnityWebRequest.EscapeURL(replyType) + "&subtipo_indicador=" + UnityWebRequest.EscapeURL(replySubType) +
			"&valor_indicador=" + UnityWebRequest.EscapeURL(indicVal) + "&hash=" + hash;

		print("addDialog : " + post_url);
		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No pudo agregar dialogo: " + webRequest.error);
			}
			else
			{
				print("dialogo agregado: " + webRequest.downloadHandler.text);
				Debug.Log("dialogo agregado: " + webRequest.downloadHandler.text);
			}
		}
	}

	public IEnumerator AddMission(int level, string missions)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(secretKey);
		addMission_URL = URL + "/missions/add?";
		string post_url = addMission_URL + "&level_id=" + level + "&missions=" + UnityWebRequest.EscapeURL(missions) + "&hash=" + hash; ;

		print("addMission : " + post_url);
		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No pudo agregar mision: " + webRequest.error);
			}
			else
			{
				print("mision agregada: " + webRequest.downloadHandler.text);
				Debug.Log("mision agregada: " + webRequest.downloadHandler.text);
			}
		}
	}

	public IEnumerator AddLevelData(int level, string missions, int portal, int fire, int pollution, int portalCharge, int fireCharge,
		int pollutionCharge, int resourcesCharge)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(secretKey);
		StringBuilder builder = new StringBuilder();
		addLevelData_URL = URL + "/levelData/add?";

		string post_url;
		builder.Append(addLevelData_URL);
		builder.Append("&level_id=");
		builder.Append(level);
		builder.Append("&missions=");
		builder.Append(UnityWebRequest.EscapeURL(missions));
		builder.Append("&portal=");
		builder.Append(portal);
		builder.Append("&fire=");
		builder.Append(fire);
		builder.Append("&pollution=");
		builder.Append(pollution);
		builder.Append("&portalCharge=");
		builder.Append(portalCharge);
		builder.Append("&fireCharge=");
		builder.Append(fireCharge);
		builder.Append("&pollutionCharge=");
		builder.Append(pollutionCharge);
		builder.Append("&resourcesCharge=");
		builder.Append(resourcesCharge);
		builder.Append("&hash=");
		builder.Append(hash);

		post_url = builder.ToString();

		print("addMission : " + post_url);
		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No pudo agregar mision: " + webRequest.error);
				pendingMessages.Add(post_url);
			}
			else
			{
				print("mision agregada: " + webRequest.downloadHandler.text);
				Debug.Log("mision agregada: " + webRequest.downloadHandler.text);
				sendMessages.Add(post_url);
			}
		}
	}

	public IEnumerator SaveSelfie(string _userid, string _compid, int level, string emoji, string estado)
	{
		string URL = Data.Instance.serverURL;
		string hash = Md5Test.Md5Sum(_userid.ToUpper() + _compid + secretKey);
		addSelfie_URL = URL + "/selfie/add?";
		string post_url;

		StringBuilder builder = new StringBuilder();
		builder.Append(addSelfie_URL);
		builder.Append("&user_id=");
		builder.Append(UnityWebRequest.EscapeURL(_userid.ToUpper()));
		builder.Append("&computer_id=");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&game_id=");
		builder.Append(gameid.ToString());
		builder.Append("-");
		builder.Append(UnityWebRequest.EscapeURL(_compid));
		builder.Append("&level_id=");
		builder.Append(level);
		builder.Append("&emoji=");
		builder.Append(UnityWebRequest.EscapeURL(emoji));
		builder.Append("&estado=");
		builder.Append(UnityWebRequest.EscapeURL(estado));
		builder.Append("&hash=");
		builder.Append(hash);

		post_url = builder.ToString();
		print("addReply : " + post_url);

		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError(webRequest.error);
				pendingMessages.Add(post_url);
			}
			else
			{
				print("reply agregada: " + webRequest.downloadHandler.text);
				Debug.Log("reply agregada: " + webRequest.downloadHandler.text);
				sendMessages.Add(post_url);
			}
		}
	}

	public IEnumerator GetCursos(System.Action<string> result)
	{
		string URL = Data.Instance.serverURL;
		getCursos_URL = URL + "/cursos";

		print("getCursos : " + getCursos_URL);

		using (UnityWebRequest webRequest = UnityWebRequest.Get(getCursos_URL))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No se pueden obtener los cursos: " + webRequest.error);
			}
			else
			{
				result(webRequest.downloadHandler.text);
				print("reply agregada: " + webRequest.downloadHandler.text);
				Debug.Log("reply agregada: " + webRequest.downloadHandler.text);
			}
		}
	}

	/// <summary>
	/// Verificacion de usuario dentro de la DB
	/// </summary>
	/// <param name="user">String con el userID</param>
	/// <param name="password">String con la contraseña</param>
	/// <param name="result">Bool que devuelve la DB si el usuario existe en la misma</param>
	/// <returns></returns>
	public IEnumerator CheckUser(string username)
	{
		string URL = Data.Instance.serverURL;
		checkUser_URL = URL + "/users/findById?";

		StringBuilder builder = new StringBuilder();
		builder.Append(checkUser_URL);
		builder.Append("user_id=");
		builder.Append(username.ToUpper());

		string post_url = builder.ToString();
		string message = "Conectando con el servidor...";

		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				message = "Error de comunicación con el Servidor.";
			}
			if (webRequest.isHttpError)
			{
				message = "Error de comunicación";
				if (webRequest.responseCode == 404)
				{
					message = "[404] Error, no se encontra la url del servidor";
				}
				if (webRequest.responseCode == 401 || webRequest.responseCode == 403)
				{
					message = "[401] Error, el codigo de usuario es invalido.";
				}
				if (webRequest.responseCode >= 500)
				{
					message = "[" + webRequest.responseCode.ToString() + "] Error en el servidor";
				}
				Debug.LogError("Error: " + webRequest.error);
			}

			//Pasar datos de la escuela
			Data.Instance.cue = "";
			Data.Instance.grado = "";
			Data.Instance.division = "";
			Data.Instance.turno = "";
			//Cargar datos del usuario
			Data.Instance.userId = username.ToUpper();
			Data.Instance.dataController.UserID = username.ToUpper();
			Data.Instance.avatarData.UserID = username.ToUpper();

			yield return message;
		}
	}

	/// <summary>
	///     Rutina que se encarga de realizar intentos de envio de información pendiente.
	/// </summary>
	/// <returns></returns>
	public void RetrySendData()
	{
		//Selfies, Dialogos o Información de Juego sin enviar.
		StartCoroutine(sendPendings());
	}

	/// <summary>
	/// Rutina que intenta enviar todos los datos pendientes independientemente
	/// 		del archivo que le pasemos como parametro.
	/// </summary>
	/// <param name="path">path al archivo de datos pendientes.</param>
	/// <returns></returns>
	IEnumerator sendPendings()
	{

		string[] allPendings = new string[0];

		foreach (string post_url in pendingMessages)
		{
			if (sendMessages.Contains(post_url))
			{
				pendingMessages.Remove(post_url);
			}
			else
			{
				using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
				{
					// Request and wait for the desired page.
					yield return webRequest.SendWebRequest();

					if (webRequest.isNetworkError || webRequest.isHttpError)
					{
						Debug.LogError("No se pudo enviar la información: " + webRequest.error);
					}
					else
					{
						print("Información enviada: " + webRequest.downloadHandler.text);
						Debug.Log("Información enviada: " + webRequest.downloadHandler.text);
						sendMessages.Add(post_url);
						pendingMessages.Remove(post_url);
					}
				}
			}
		}
	}

	/// <summary>
	///  Verifica contra la DB si la sesión local es la mas reciente que la que tiene el servidor.
	///     En caso de ser así se actualiza el servidor sino se actualiza la local.
	/// </summary>
	/// <param name="gameData">Informacion de la partida local</param>
	/// <returns></returns>
	public IEnumerator VerifyLastSession(string gameData)
	{
		string URL = Data.Instance.serverURL;
		getSession_URL = URL + "";
		StringBuilder builder = new StringBuilder();

		builder.Append(getSession_URL);
		builder.Append("gameData");
		builder.Append(gameData);

		string post_url = builder.ToString();

		using (UnityWebRequest webRequest = UnityWebRequest.Get(post_url))
		{
			// Request and wait for the desired page.
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError || webRequest.isHttpError)
			{
				Debug.LogError("No existe sesión web" + webRequest.error);
			}
			else
			{
				print("Existe una sesión web!" + webRequest.downloadHandler.text);
				Debug.LogError("Existe una sesión web!" + webRequest.downloadHandler.text);
			}
		}
	}
}