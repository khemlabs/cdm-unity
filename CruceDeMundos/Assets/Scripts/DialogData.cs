﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using SimpleJSON;
using System.IO;
using System.Text;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class DialogData : MonoBehaviour
{
	public bool sendDialogs2Database;
	public DialogCharacter[] dialogCharacters;
	public List<Dialog> dialogs;
	//bool modifyDialogIndex = true;
	public string PATH = "/Dialogs";

	void Start()
	{
		dialogs = new List<Dialog>();
		//modifyDialogIndex = true;
		//En el futuro borrar la variable dir ya que al ser en WebGL no tiene sentido.
#if UNITY_WEBGL && !UNITY_EDITOR
		string URL = Data.Instance.baseURL;
		List<string> dialogsNames = dialogsList();
		foreach (String filename in dialogsNames)
		{
			StartCoroutine(Import(URL + "/Dialogs/" + filename));
		}
#else

#if UNITY_STANDALONE_OSX && !UNITY_EDITOR
		DirectoryInfo dir = new DirectoryInfo(Directory.GetParent(Directory.GetParent(Application.dataPath).FullName).FullName+PATH);
#else
		DirectoryInfo dir = new DirectoryInfo(Directory.GetParent(Application.dataPath).FullName + PATH);
#endif
		FileInfo[] info = dir.GetFiles("*.json");

		foreach (FileInfo f in info)
		{
			if (!f.FullName.Contains("fulldialogs"))
			{
				StartCoroutine(Import(f.FullName));
			}
		}
#endif
	}
	/*
		private void Update()
		{/*
		#if UNITY_EDITOR
				//Genera archivo con todos los dialogos de la DB
				if (modifyDialogIndex && dialogs.Count >= 30)
				{
					CreateDBDialog();
				}
				if (dialogs.Count < 30) modifyDialogIndex = true;
		#endif

		}*/

	List<string> dialogsList()
	{
		List<string> dialogsNames = new List<string>();
		dialogsNames.Add("Agus_level_2.json");
		dialogsNames.Add("Agus_level_6.json");
		dialogsNames.Add("Bombero2_level_2.json");
		dialogsNames.Add("Bombero3_level_1.json");
		dialogsNames.Add("Bombero3_level_6.json");
		dialogsNames.Add("Bombero_level_4.json");
		dialogsNames.Add("DraGrimberg_level_1.json");
		dialogsNames.Add("DraGrimberg_level_2.json");
		dialogsNames.Add("DraGrimberg_level_3.json");
		dialogsNames.Add("DraGrimberg_level_4.json");
		dialogsNames.Add("DraGrimberg_level_5.json");
		dialogsNames.Add("DraGrimberg_level_6.json");
		dialogsNames.Add("DraGrimberg_level_7.json");
		dialogsNames.Add("Ema_level_2.json");
		dialogsNames.Add("Ema_level_7.json");
		dialogsNames.Add("Mark_level_1.json");
		dialogsNames.Add("Mark_level_3.json");
		dialogsNames.Add("Merk_level_2.json");
		dialogsNames.Add("Merk_level_5.json");
		dialogsNames.Add("Mindy_level_5.json");
		dialogsNames.Add("Mirk_level_1.json");
		dialogsNames.Add("Mirk_level_2.json");
		dialogsNames.Add("Mirk_level_6.json");
		dialogsNames.Add("Morkid_level_5.json");
		dialogsNames.Add("Mork_level_3.json");
		dialogsNames.Add("Mork_level_5.json");
		dialogsNames.Add("Mork_level_6.json");
		dialogsNames.Add("Murk_level_3.json");
		dialogsNames.Add("Murk_level_5.json");
		dialogsNames.Add("Patovalien_level_5.json");
		return dialogsNames;
	}

	/// <summary>
	///  Vamos a buscar un dialogo a partir de saber si es inicial, final o el nombre de un personaje
	/// </summary>
	/// <param name="initial">Es inicial?</param>
	/// <param name="final">Es final?</param>
	/// <param name="level">Nivel</param>
	/// <param name="name">Personaje</param>
	/// <returns></returns>
	public Dialog findDialog(bool initial, bool final, int level, string name)
	{
		Dialog dialog = new Dialog();

		if (initial) dialogs.Find(x => (x.initial == true && x.level == level));
		else
		{
			if (final) dialogs.Find(x => (x.final == true && x.level == level));
			else
			{
				if (final) dialogs.Find(x => (x.name == name && x.level == level));
			}
		}

		return dialog;
	}


	[Serializable]
	public class DialogCharacter
	{
		public string name;
		[JsonIgnore]
		public GameObject visualization;
		public int lastEmoVal;
		public int globalEmoVal;
		public List<LevelInfo> levelsInfo;
		[Serializable]
		public class LevelInfo
		{
			public int level;
			public int emoval;
			public int goTo;
			public string lastExpre;
			[JsonIgnore]
			public string dtype;
		}

		public void ResetLevel(int level)
		{
			LevelInfo l = levelsInfo.Find(x => x.level == level);
			if (l != null)
			{
				l.emoval = 0;
				l.goTo = 0;
			}
		}

		public void ResetType(int level, string dtype)
		{
			LevelInfo l = levelsInfo.Find(x => x.level == level && x.dtype == dtype);
			if (l != null)
			{
				l.emoval = 0;
				l.goTo = 0;
			}
		}
	}

	[JsonObject(MemberSerialization.OptIn)]
	public class DialogResponseInfo
	{
		[JsonProperty]
		public DialogCharacter[] dialogCharactersResponse;

		public DialogResponseInfo(DialogCharacter[] dialogCharactersResponse)
		{
			this.dialogCharactersResponse = dialogCharactersResponse;
		}
	}

	public void ResetAllAtLevel(int level)
	{
		foreach (DialogCharacter dch in dialogCharacters)
			dch.ResetLevel(level);
	}

	public void ResetHintAtLevel(int level)
	{
		foreach (DialogCharacter dch in dialogCharacters)
		{
			dch.ResetType(level, "AUTOEVAL");
			dch.ResetType(level, "COLLAB");
		}
	}

	IEnumerator Import(string file)
	{
#if UNITY_WEBGL && !UNITY_EDITOR
		UnityWebRequest www = UnityWebRequest.Get(file);
#else
		UnityWebRequest www = UnityWebRequest.Get("file://" + file);
#endif

		yield return www.SendWebRequest();
		string text = "";

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{
			text = www.downloadHandler.text;
		}

		JSONNode N = JSON.Parse(text);

		Dialog d = new Dialog
		{
			name = N["name"],
			level = N["level"].AsInt,
			dialogType = CastDialogType(N["type"]),
			initial = N["initial"] != null ? N["initial"].AsBool : false,
			final = N["final"] != null ? N["final"].AsBool : false,
			dialogTree = new Dialog.DialogTree[N["dialogTree"].Count]
		};
		for (int i = 0; i < d.dialogTree.Length; i++)
		{
			d.dialogTree[i] = new Dialog.DialogTree
			{
				index = N["dialogTree"][i]["index"].AsInt,
				moods = new Dialog.Mood[N["dialogTree"][i]["moods"].Count]
			};
			for (int j = 0; j < d.dialogTree[i].moods.Length; j++)
			{
				d.dialogTree[i].moods[j] = new Dialog.Mood
				{
					mType = CastMoodType(N["dialogTree"][i]["moods"][j]["mood"]),
					final = N["dialogTree"][i]["moods"][j]["final"].AsBool,
					prompt = N["dialogTree"][i]["moods"][j]["prompt"]
				};
				if (N["dialogTree"][i]["moods"][j]["expre"] != null)
					d.dialogTree[i].moods[j].expre = N["dialogTree"][i]["moods"][j]["expre"];
				else
					d.dialogTree[i].moods[j].expre = N["dialogTree"][i]["moods"][j]["expre"] = "";
				d.dialogTree[i].moods[j].replies = new Dialog.Reply[N["dialogTree"][i]["moods"][j]["replies"].Count];
				for (int k = 0; k < d.dialogTree[i].moods[j].replies.Length; k++)
				{
					d.dialogTree[i].moods[j].replies[k] = new Dialog.Reply
					{
						emoVal = N["dialogTree"][i]["moods"][j]["replies"][k]["emoVal"].AsInt,
						exit = N["dialogTree"][i]["moods"][j]["replies"][k]["exit"].AsBool,
						goTo = N["dialogTree"][i]["moods"][j]["replies"][k]["goTo"].AsInt,
						text = N["dialogTree"][i]["moods"][j]["replies"][k]["text"],
						dialogId = 0
					};

					string rt = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["rType"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].replyType = CastReplyType(N["dialogTree"][i]["moods"][j]["replies"][k]["rType"]);
						rt = N["dialogTree"][i]["moods"][j]["replies"][k]["rType"];
						rt = rt.Replace(",", ";");
					}

					string rst = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].replySubType = CastReplySubType(N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"]);
						rst = N["dialogTree"][i]["moods"][j]["replies"][k]["rSType"];
						rst = rst.Replace(",", ";");
					}

					string iv = "";
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["indicVal"] != null)
					{
						iv = N["dialogTree"][i]["moods"][j]["replies"][k]["indicVal"];
						d.dialogTree[i].moods[j].replies[k].indicadorVal = iv.Split(',');
						iv = iv.Replace(",", ";");
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["resources"] != null)
						d.dialogTree[i].moods[j].replies[k].resources = N["dialogTree"][i]["moods"][j]["replies"][k]["resources"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].resources = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["fireCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].fireCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["fireCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].fireCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["portalCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].portalCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["portalCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].portalCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["pollutionCharge"] != null)
						d.dialogTree[i].moods[j].replies[k].pollutionCharge = N["dialogTree"][i]["moods"][j]["replies"][k]["pollutionCharge"].AsInt;
					else
						d.dialogTree[i].moods[j].replies[k].pollutionCharge = 0;

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["tool"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].tool = N["dialogTree"][i]["moods"][j]["replies"][k]["tool"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].tool = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["objective"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].objective = N["dialogTree"][i]["moods"][j]["replies"][k]["objective"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].objective = false;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["dialog"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].dialog = N["dialogTree"][i]["moods"][j]["replies"][k]["dialog"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].dialog = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["levelEnd"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].levelEndDialog = N["dialogTree"][i]["moods"][j]["replies"][k]["levelEnd"].AsBool;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].levelEndDialog = false;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["move"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].move = N["dialogTree"][i]["moods"][j]["replies"][k]["move"].AsInt;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].move = -1;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["block"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].block = N["dialogTree"][i]["moods"][j]["replies"][k]["block"].AsInt;
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].block = 0;
					}

					LevelData.DialogUnlock du = new LevelData.DialogUnlock();
					if (N["dialogTree"][i]["moods"][j]["replies"][k]["unlock"] != null)
					{
						string s = N["dialogTree"][i]["moods"][j]["replies"][k]["unlock"];
						string[] ss = s.Split(',');
						du.characterName = ss[0];
						du.goTo = int.Parse(ss[1]);
						d.dialogTree[i].moods[j].replies[k].dUnlock = du;
					}
					else
					{
						du.characterName = "";
						du.goTo = -1;
						d.dialogTree[i].moods[j].replies[k].dUnlock = du;
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["obstacle"] != null)
					{
						d.dialogTree[i].moods[j].replies[k].oType = N["dialogTree"][i]["moods"][j]["replies"][k]["obstacle"];
					}
					else
					{
						d.dialogTree[i].moods[j].replies[k].oType = "";
					}

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["friendDisable"] != null)
						d.dialogTree[i].moods[j].replies[k].friendDisable = N["dialogTree"][i]["moods"][j]["replies"][k]["friendDisable"];
					else
						d.dialogTree[i].moods[j].replies[k].friendDisable = "";

					if (N["dialogTree"][i]["moods"][j]["replies"][k]["friendAdd"] != null)
						d.dialogTree[i].moods[j].replies[k].friendAdd = N["dialogTree"][i]["moods"][j]["replies"][k]["friendAdd"].AsBool;
					else
						d.dialogTree[i].moods[j].replies[k].friendAdd = false;

					if (sendDialogs2Database)
					{
						yield return new WaitForSeconds(1f);
						if (d.level > 0)
							StartCoroutine(Data.Instance.dataController.AddDialog(d.name, d.level, d.dialogTree[i].index,
								d.dialogType.ToString(), d.dialogTree[i].moods[j].mType.ToString(), d.dialogTree[i].moods[j].prompt,
								k, d.dialogTree[i].moods[j].replies[k].text, rt, rst, iv));
					}
				}
			}
		}

		yield return d;
		dialogs.Add(d);
	}


	[Serializable]
	public class Dialog
	{
		public enum DType
		{
			AUTOEVAL,
			NARRATIVE,
			COLLAB,
			ET,
			HUMAN
		}
		public string name;
		public int level;
		public string dialogType;
		public bool initial;
		public bool final;
		public DialogTree[] dialogTree;
		[Serializable]
		public class DialogTree
		{
			public int index;
			public Mood[] moods;

		}
		[Serializable]
		public class Mood
		{
			[Serializable]
			public enum MoodType
			{
				NEGATIVE,
				NEUTRAL,
				POSITIVE
			}
			public MoodType mType;
			public string expre;
			public string prompt;
			public bool final;
			public Reply[] replies;
		}
		[Serializable]
		public class Reply
		{
			public int dialogId;
			public int emoVal;
			public bool exit;
			public int goTo;
			public string text;
			public List<RType> replyType;
			public List<RSubType> replySubType;
			public string[] indicadorVal;
			public int resources;
			public int fireCharge;
			public int portalCharge;
			public int pollutionCharge;
			public string tool;
			public bool objective;
			public string dialog;
			public bool levelEndDialog;
			public int move;
			public int block;
			public LevelData.DialogUnlock dUnlock;
			public string oType;
			public string friendDisable;
			public bool friendAdd;
			[Serializable]
			public enum RType
			{
				NARRATIVO,
				ASERTIVIDAD,
				AUTOEFICACIA,
				COLABORATIVO,
				EMPATÍA
			}

			[Serializable]
			public enum RSubType
			{
				NARRATIVO,
				PROACTIVIDAD,//asertividad
				INTERÉS,
				PEDIDO,
				NEGARSE,
				JUICIO,//autoeficacia
				AUTOPERCEPCIÓN,
				COMPARTIR,//colaborativo
				ACEPTAR,
				CONFIANZA,
				TODOS,
				RECONOCIMIENTO,//empatía
				ACCIÓN
			}
		}
	}

	Dialog.Mood.MoodType CastMoodType(string s)
	{
		return (Dialog.Mood.MoodType)System.Enum.Parse(typeof(Dialog.Mood.MoodType), s.ToUpperInvariant());
	}

	string CastDialogType(string s)
	{
		if (s != null)
		{
			return s.ToUpper();
		}
		else
		{
			return "NARRATIVE";
		}
	}


	List<Dialog.Reply.RType> CastReplyType(string s)
	{
		List<Dialog.Reply.RType> l = new List<Dialog.Reply.RType>();
		string[] sl = s.Split(',');
		foreach (string st in sl)
			l.Add((Dialog.Reply.RType)System.Enum.Parse(typeof(Dialog.Reply.RType), st.ToUpperInvariant()));
		return l;
	}

	List<Dialog.Reply.RSubType> CastReplySubType(string s)
	{
		List<Dialog.Reply.RSubType> l = new List<Dialog.Reply.RSubType>();
		string[] sl = s.Split(',');
		foreach (string st in sl)
			l.Add((Dialog.Reply.RSubType)System.Enum.Parse(typeof(Dialog.Reply.RSubType), st.ToUpperInvariant()));
		return l;
	}

	public DialogResponseInfo GetDialogData()
	{

		DialogResponseInfo dialogResponseInfo = new DialogResponseInfo(dialogCharacters);

		return dialogResponseInfo;
	}

	public void SetDialogData(DialogResponseInfo dialogResponseInfo)
	{
		DialogCharacter[] dialogCharactersResponse = dialogResponseInfo.dialogCharactersResponse;
		for (int i = 0; i < dialogCharactersResponse.Length; i++)
		{
			dialogCharacters[i].levelsInfo = dialogCharactersResponse[i].levelsInfo;
			dialogCharacters[i].lastEmoVal = dialogCharactersResponse[i].lastEmoVal;
			dialogCharacters[i].globalEmoVal = dialogCharactersResponse[i].globalEmoVal;
		}
	}

	/// <summary>
	///     Recorre la colección de dialogos y genera el formato necesario para la DB
	/// </summary>
	public void CreateDBDialog()
	{
		int index = 0;
		StringBuilder builder = new StringBuilder();

		//Recorrido de los dialogos
		foreach (Dialog dialog in dialogs)
		{
			foreach (var leaf in dialog.dialogTree)
			{
				foreach (var mood in leaf.moods)
				{
					int answerIndx = 0;
					foreach (var reply in mood.replies)
					{
						DialogDB dialogDB = new DialogDB
						{
							id = index,
							character_name = dialog.name,
							level_id = dialog.level,
							dialog_type = dialog.dialogType.ToString(),
							dialog_index = leaf.index,
							dialog_mood = mood.mType.ToString(),
							dialog_prompt = mood.prompt,
							answer_id = answerIndx,
							answer_text = reply.text,
							create_time = DateTime.Now.ToString()
						};
						reply.dialogId = index;

						if (reply.replyType != null)
						{
							dialogDB.tipo_indicador = reply.replyType[0].ToString();
						}
						else
						{
							dialogDB.tipo_indicador = "";
						}

						if (reply.replySubType != null)
						{
							dialogDB.subtipo_indicador = reply.replySubType[0].ToString();
						}
						else
						{
							dialogDB.subtipo_indicador = "";
						}

						if (reply.indicadorVal != null)
						{
							dialogDB.valor_indicador = reply.indicadorVal[0].ToString();
						}
						else
						{
							dialogDB.valor_indicador = "";
						}

						index++;
						answerIndx++;

						//Se agrega los dialogos con formato DB
						builder.Append("(");
						builder.Append(dialogDB.id.ToString());
						builder.Append(",'");
						builder.Append(dialogDB.character_name);
						builder.Append("',");
						builder.Append(dialogDB.level_id.ToString());
						builder.Append(",'");
						builder.Append(dialogDB.dialog_type);
						builder.Append("',");
						builder.Append(dialogDB.dialog_index.ToString());
						builder.Append(",'");
						builder.Append(dialogDB.dialog_mood);
						builder.Append("','");
						builder.Append(dialogDB.dialog_prompt);
						builder.Append("',");
						builder.Append(dialogDB.answer_id);
						builder.Append(",'");
						builder.Append(dialogDB.answer_text);
						builder.Append("','");
						builder.Append(dialogDB.create_time);
						builder.Append("','");
						builder.Append(dialogDB.tipo_indicador);
						builder.Append("','");
						builder.Append(dialogDB.subtipo_indicador);
						builder.Append("','");
						builder.Append(dialogDB.valor_indicador);
						builder.Append("'),\n");
					}
				}
			}
		}


		string path = "." + PATH + "/fulldialogs.json";
		//Se guardan los datos generados
		if (!File.Exists(path))
		{
			File.WriteAllText(path, builder.ToString());
		}
		else
		{
			File.WriteAllText(path, builder.ToString());
		}

		//modifyDialogIndex = false;
	}

	[Serializable]
	public class DialogDB
	{
		public int id;
		public string character_name;
		public int level_id;
		public string dialog_type;
		public int dialog_index;
		public string dialog_mood;
		public string dialog_prompt;
		public int answer_id;
		public string answer_text;
		public string create_time;
		public string tipo_indicador;
		public string subtipo_indicador;
		public string valor_indicador;
	}
}
