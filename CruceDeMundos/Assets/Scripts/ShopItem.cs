﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{

	public PlayerData.ToolName toolName;
	public int level = 0;

	public int val;

	// Use this for initialization
	void Start()
	{
		val = 0;
		if (Data.Instance.playerData.tools[(int)toolName].GetComponent<ToolData>().levels[level] != null)
		{

			val = Data.Instance.playerData.tools[(int)toolName].GetComponent<ToolData>().levels[level].cost;
		}
	}

	public void OnPointerEnter(int index)
	{
		Data.Instance.interfaceSfx.PlaySfx(Data.Instance.interfaceSfx.over);
		Events.OnToolButtonEnter(index);
	}

	public void OnPointerExit()
	{
		Events.OnToolButtonExit();
	}
}
