﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ShadowEffect : MonoBehaviour
{
	public Vector2 Offset = new Vector2(-5f, -5f);
	public Material Material;
	public Color ShadowColor;
	private SpriteRenderer spriteRenderCaster;
	private SpriteRenderer spriteRendererShadow;
	private Transform transformCaster;
	private Transform transformShadow;

	void Start()
	{
		transformCaster = transform;
		transformShadow = new GameObject("Shadow").transform;
		transformShadow.parent = transformCaster;

		spriteRenderCaster = GetComponent<SpriteRenderer>();
		spriteRendererShadow = transformShadow.gameObject.AddComponent<SpriteRenderer>();

		transformShadow.position = new Vector2(transformCaster.position.x + Offset.x, transformCaster.position.y + Offset.y);

		spriteRendererShadow.material = Material;
		spriteRendererShadow.color = ShadowColor;
		spriteRendererShadow.sprite = spriteRenderCaster.sprite;
		spriteRendererShadow.sortingLayerName = spriteRenderCaster.sortingLayerName;
		spriteRendererShadow.sortingOrder = spriteRenderCaster.sortingOrder - 1;
	}

}
